//*****************************************************************************
//
// usb_bulk_structs.c - Data structures defining this bulk USB device.
//
// Copyright (c) 2008-2010 Texas Instruments Incorporated.  All rights reserved.
// Software License Agreement
//
// Texas Instruments (TI) is supplying this software for use solely and
// exclusively on TI's microcontroller products. The software is owned by
// TI and/or its suppliers, and is protected under applicable copyright
// laws. You may not combine this software with "viral" open-source
// software in order to form a larger program.
//
// THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
// NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
// NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. TI SHALL NOT, UNDER ANY
// CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
// DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 6288 of the DK-LM3S9B96 Firmware Package.
//
//*****************************************************************************
#include <string.h>
#include "hw_types.h"
#include "interrupt.h"
#include "usb.h"
#include "usblib.h"
#include "usb-ids.h"
#include "usbdevice.h"
#include "usbdbulk.h"
#include "usb_bulk_structs.h"
#include "hw_usbphyGS60.h"
#ifdef DMA_MODE
#include "cppi41dma.h"
#endif

#define RX_CMD_BUFER_LEN            (320)

extern unsigned char TxEndFlag;
int RxCpl;

unsigned char   RxCommandBuffer[RX_CMD_BUFER_LEN];


unsigned int RxHandler(void *pvCBData, unsigned int ulEvent, unsigned int ulMsgValue, void *pvMsgData);
void RxBufferCopy(unsigned char *pcData, unsigned int ulMsgValue);
unsigned int TxHandler(void *pvCBData, unsigned int ulEvent, unsigned int ulMsgValue, void *pvMsgData);


//*****************************************************************************
//
// The languages supported by this device.
//
//*****************************************************************************
const unsigned char g_pLangDescriptor[] =
{
	4,
	USB_DTYPE_STRING,
	USBShort(USB_LANG_EN_US)
};

//*****************************************************************************
//
// The manufacturer string.
//
//*****************************************************************************
const unsigned char g_pManufacturerString[] =
{
	(17 + 1) * 2,
	USB_DTYPE_STRING,
	'T', 0, 'e', 0, 'x', 0, 'a', 0, 's', 0, ' ', 0, 'I', 0, 'n', 0, 's', 0,
	't', 0, 'r', 0, 'u', 0, 'm', 0, 'e', 0, 'n', 0, 't', 0, 's', 0,
};

//*****************************************************************************
//
// The product string.
//
//*****************************************************************************
const unsigned char g_pProductString[] =
{
	(19 + 1) * 2,
	USB_DTYPE_STRING,
	'G', 0, 'e', 0, 'n', 0, 'e', 0, 'r', 0, 'i', 0, 'c', 0, ' ', 0, 'B', 0,
	'u', 0, 'l', 0, 'k', 0, ' ', 0, 'D', 0, 'e', 0, 'v', 0, 'i', 0, 'c', 0,
	'e', 0
};

//*****************************************************************************
//
// The serial number string.
//
//*****************************************************************************
const unsigned char g_pSerialNumberString[] =
{
	(8 + 1) * 2,
	USB_DTYPE_STRING,
	'1', 0, '2', 0, '3', 0, '4', 0, '5', 0, '6', 0, '7', 0, '8', 0
};

//*****************************************************************************
//
// The data interface description string.
//
//*****************************************************************************
const unsigned char g_pDataInterfaceString[] =
{
	(19 + 1) * 2,
	USB_DTYPE_STRING,
	'B', 0, 'u', 0, 'l', 0, 'k', 0, ' ', 0, 'D', 0, 'a', 0, 't', 0,
	'a', 0, ' ', 0, 'I', 0, 'n', 0, 't', 0, 'e', 0, 'r', 0, 'f', 0,
	'a', 0, 'c', 0, 'e', 0
};

//*****************************************************************************
//
// The configuration description string.
//
//*****************************************************************************
const unsigned char g_pConfigString[] =
{
	(23 + 1) * 2,
	USB_DTYPE_STRING,
	'B', 0, 'u', 0, 'l', 0, 'k', 0, ' ', 0, 'D', 0, 'a', 0, 't', 0,
	'a', 0, ' ', 0, 'C', 0, 'o', 0, 'n', 0, 'f', 0, 'i', 0, 'g', 0,
	'u', 0, 'r', 0, 'a', 0, 't', 0, 'i', 0, 'o', 0, 'n', 0
};

//*****************************************************************************
//
// The descriptor string table.
//
//*****************************************************************************
const unsigned char * const g_pStringDescriptors[] =
{
	g_pLangDescriptor,
	g_pManufacturerString,
	g_pProductString,
	g_pSerialNumberString,
	g_pDataInterfaceString,
	g_pConfigString
};

#define NUM_STRING_DESCRIPTORS (sizeof(g_pStringDescriptors) /			\
									sizeof(unsigned char *))

//*****************************************************************************
//
// The bulk device initialization and customization structures. In this case,
// we are using USBBuffers between the bulk device class driver and the
// application code. The function pointers and callback data values are set
// to insert a buffer in each of the data channels, transmit and receive.
//
// With the buffer in place, the bulk channel callback is set to the relevant
// channel function and the callback data is set to point to the channel
// instance data. The buffer, in turn, has its callback set to the application
// function and the callback data set to our bulk instance structure.
//
//*****************************************************************************
tBulkInstance g_sBulkInstance;

const tUSBBuffer g_sTxBuffer;
const tUSBBuffer g_sRxBuffer;

const tUSBDBulkDevice g_sBulkDevice =
{
	USB_VID_STELLARIS,
	USB_PID_BULK,
	500,
	USB_CONF_ATTR_SELF_PWR,
	USBBufferEventCallback,
	(void *)&g_sRxBuffer,
	USBBufferEventCallback,
	(void *)&g_sTxBuffer,
	g_pStringDescriptors,
	NUM_STRING_DESCRIPTORS,
	&g_sBulkInstance
};

//*****************************************************************************
//
// The size of the transmit and receive buffers used. 256 is chosen pretty
// much at random though the buffer should be at least twice the size of
// a maximum-sized USB packet.
//
//*****************************************************************************
//#define BULK_BUFFER_SIZE 5504000//3354624//6752256//81920*2+1//40960*2+1//6752256////18432

//*****************************************************************************
//
// Receive buffer (from the USB perspective).
//
//*****************************************************************************
unsigned char g_pucUSBRxBuffer[USB_PACKET_LENGTH];
unsigned char g_pucRxBufferWorkspace[USB_BUFFER_WORKSPACE_SIZE];
const tUSBBuffer g_sRxBuffer =
{
	false,							// This is a receive buffer.
	RxHandler,						// pfnCallback
	(void *)&g_sBulkDevice,		// Callback data is our device pointer.
	USBDBulkPacketRead,			// pfnTransfer
	USBDBulkRxPacketAvailable,		// pfnAvailable
	(void *)&g_sBulkDevice,			// pvHandle
	g_pucUSBRxBuffer,			// pcBuffer
    USB_PACKET_LENGTH,								// ulBufferSize
	g_pucRxBufferWorkspace			// pvWorkspace
};

//*****************************************************************************
//
// Transmit buffer (from the USB perspective).
//
//*****************************************************************************
//unsigned char g_pucUSBTxBuffer[BULK_BUFFER_SIZE];
unsigned char g_pucUSBTxBuffer[USB_PACKET_LENGTH];
unsigned char g_pucTxBufferWorkspace[USB_BUFFER_WORKSPACE_SIZE];
const tUSBBuffer g_sTxBuffer =
{
	true,								// This is a transmit buffer.
	TxHandler,						// pfnCallback
	(void *)&g_sBulkDevice,		// Callback data is our device pointer.
	USBDBulkPacketWrite,			// pfnTransfer
	USBDBulkTxPacketAvailable,	// pfnAvailable
	(void *)&g_sBulkDevice,		// pvHandle
	g_pucUSBTxBuffer,			// pcBuffer
    USB_PACKET_LENGTH,
	g_pucTxBufferWorkspace		// pvWorkspace
};

#ifdef DMA_MODE
endpointInfo epInfo[]=
{
    {
	    USB_EP_TO_INDEX(USB_EP_1),
	    CPDMA_DIR_TX,
	    CPDMA_MODE_SET_GRNDIS,
    }
};
#endif

void USBInit(void)
{
	IntRegister(SYS_INT_USB0, USB0DeviceIntHandler);
	IntChannelSet(SYS_INT_USB0, 2);
	IntSystemEnable(SYS_INT_USB0);

	USBBufferInit((tUSBBuffer *)&g_sRxBuffer);
	USBBufferInit((tUSBBuffer *)&g_sTxBuffer);
	USBDBulkInit(0, (tUSBDBulkDevice *)&g_sBulkDevice);

#ifdef DMA_MODE
	Cppi41DmaInit(USB_INSTANCE, epInfo, 1);
#endif
}

unsigned int RxHandler(void *pvCBData, unsigned int ulEvent, unsigned int ulMsgValue, void *pvMsgData)
{
	switch (ulEvent) {
	case USB_EVENT_CONNECTED:
		USBBufferFlush(&g_sTxBuffer);
		USBBufferFlush(&g_sRxBuffer);
		break;

	case USB_EVENT_DISCONNECTED:		
		break;

	case USB_EVENT_RX_AVAILABLE:
		RxBufferCopy(pvMsgData, ulMsgValue);
		RxCpl = 1;
		break;

	case USB_EVENT_SUSPEND:		
		break;

	case USB_EVENT_RESUME:
		break;

	default:
		break;
	}

	return 0;
}

int GetUSBCMDIdx(unsigned short *pCMDIdx)
{
	int ret;

	if (RxCpl == 1)
	{
		*pCMDIdx = (unsigned short)(RxCommandBuffer[0] << 8 | RxCommandBuffer[1]);
		RxCpl	= 0;
		ret	= 0;
	}
	else
	{
		ret = -1;
	}

	return ret;
}

unsigned char *GetUSBArg(void)
{
    return &RxCommandBuffer[2];
}

void RxBufferCopy(unsigned char *pcData, unsigned int ulMsgValue)
{
	memcpy(RxCommandBuffer, pcData, ulMsgValue);
	USBBufferInit((tUSBBuffer *)&g_sRxBuffer);
}

unsigned int TxHandler(void *pvCBData, unsigned int ulEvent, unsigned int ulMsgValue, void *pvMsgData)
{
	return 0;
}

void SendUSBPacket(unsigned char *pPkt, unsigned int Length)
{
    enableCoreTxDMA(USB_INSTANCE, g_sBulkInstance.ucINEndpoint);
    doDmaTxTransfer(USB_INSTANCE, pPkt, Length, g_sBulkInstance.ucINEndpoint);

    /* Wait transfer completion */
    while (TxEndFlag == 0);
    TxEndFlag = 0;
}