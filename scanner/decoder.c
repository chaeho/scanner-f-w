#include <stdlib.h>
#include <string.h>
#include "hw_types.h"
#include "usb_bulk_structs.h"
#include "cis.h"
#include "decoder.h"

#include "debug.h"


#define MEDEIA_WIDTH_MM                         (70 * PIXEL_PER_MM)

#define MEDIA_DIR_UNKNOWN                       (0)
#define MEDIA_DIR_FORWARD                       (1)
#define MEDIA_DIR_BACKWARD                      (2)

#define AXIS_NULL                               (0xFFFF)

#define PIXEL_THRESHOLD                         (128)
#define NUM_DOT_EDGE_DECISION                   (6)

#define NUM_DOT_REF_MARK_DECISION               (3 * PIXEL_PER_MM)
#define NUM_DOT_REF_MARK_WIDTH                  (4 * PIXEL_PER_MM)
#define NUM_DOT_REF_USER_DISTANCE               (15 * PIXEL_PER_MM / 10) // 1.5mm
#define NUM_DOT_USER_MARK_HEIGHT                (15 * PIXEL_PER_MM / 10) // 1.5mm
#define NUM_DOT_USER_MARK_WIDTH                 (4 * PIXEL_PER_MM)
#define NUM_DOT_USER_MARK_DISTANCE              (5 * PIXEL_PER_MM)
#define MARKER_1ST_POSITION                     (6 * PIXEL_PER_MM)
#define ROUND_HEIGHT                            (5 * PIXEL_PER_MM)

#define PROCESS_Y_DEGREE                        (2)


DECODER gstDecoder;


unsigned char *ImgSeekLine(unsigned int LineNum);


void DecoderInit(unsigned char *pImgBuf, unsigned short NumDotPerRow, unsigned short NumImgPerBuf)
{
    memset(&gstDecoder, 0, sizeof(DECODER));

    gstDecoder.stEdgeTopLeft.X    = AXIS_NULL;
    gstDecoder.stEdgeTopLeft.Y    = AXIS_NULL;
    gstDecoder.stEdgeTopRight.X   = AXIS_NULL;
    gstDecoder.stEdgeTopRight.Y   = AXIS_NULL;

    gstDecoder.pImgBuf          = pImgBuf;
    gstDecoder.NumDotPerRow     = NumDotPerRow;
    gstDecoder.NumImg           = NumImgPerBuf;

    DBG_PRINT("decoding start");
}

unsigned char *ImgSeekLine(unsigned int LineNum)
{
    return gstDecoder.pImgBuf + (LineNum * gstDecoder.NumDotPerRow * gstDecoder.NumImg);
}

int DecoderFindTopEdge(unsigned int EndLine)
{
    unsigned char   *pLineFirstDot;
    unsigned char   *pLineLastDot;
    unsigned int    DotSum;
    unsigned int    DotOffset;
    unsigned int    MaxBlackSpaceWidth;
    unsigned int    Ret;

    MaxBlackSpaceWidth  = (CIS_NUM_DOT_PER_SCAN - MEDEIA_WIDTH_MM + NUM_DOT_EDGE_DECISION) * gstDecoder.NumImg;
    Ret                 = DECODER_RET_WAIT;

    while (EndLine > gstDecoder.ProcessLine + 1)
    {
        /* searching left top edge */
        if (gstDecoder.stEdgeTopLeft.Y == AXIS_NULL)
        {
            pLineFirstDot = ImgSeekLine(gstDecoder.ProcessLine);
            DotSum = 0;

            for (DotOffset = 0; DotOffset < MaxBlackSpaceWidth; DotOffset += gstDecoder.NumImg)
            {
                /* look up left to right */
                if (*(pLineFirstDot + DotOffset) > PIXEL_THRESHOLD)
                {
                    DotSum++;
                    if (DotSum > NUM_DOT_EDGE_DECISION)
                    {
                        DBG_PRINT("found left top edge position Y : %d (X %d)", gstDecoder.ProcessLine, DotOffset / gstDecoder.NumImg);

                        gstDecoder.stEdgeTopLeft.Y = gstDecoder.ProcessLine;
                        break;
                    }
                }
            }
        }

        /* searching right top edge */
        if (gstDecoder.stEdgeTopRight.Y == AXIS_NULL)
        {
            pLineLastDot = ImgSeekLine(gstDecoder.ProcessLine + 1) - gstDecoder.NumImg;
            DotSum = 0;

            for (DotOffset = 0; DotOffset < MaxBlackSpaceWidth; DotOffset += gstDecoder.NumImg)
            {
                /* look up right to left */
                if (*(pLineLastDot - DotOffset) > PIXEL_THRESHOLD)
                {
                    DotSum++;
                    if (DotSum > NUM_DOT_EDGE_DECISION)
                    {
                        DBG_PRINT("found right top edge position Y : %d X (%d)", gstDecoder.ProcessLine, gstDecoder.NumDotPerRow - (DotOffset / gstDecoder.NumImg));

                        gstDecoder.stEdgeTopRight.Y = gstDecoder.ProcessLine;
                        break;
                    }
                }
            }
        }

        gstDecoder.ProcessLine += PROCESS_Y_DEGREE;

        if ((gstDecoder.stEdgeTopLeft.Y != AXIS_NULL) &&
            (gstDecoder.stEdgeTopRight.Y != AXIS_NULL))
        {
            Ret = DECODER_RET_SUCCESS;
            break;
        }
    }

    return Ret;
}

int DecoderFindLeftRightEdge(unsigned int EndLine)
{
    unsigned char   *pLineLeftDot;
    unsigned char   *pLineRightDot;
    unsigned int    DotOffset;
    unsigned int    Ret;
    unsigned int    FoundLeft;
    unsigned int    FoundRight;
    unsigned int    SearchCnt;
    unsigned int    SearchLimit;

    Ret = DECODER_RET_WAIT;

    if (EndLine < gstDecoder.ProcessLine + 1)
    {
        DBG_PRINT("%d/%d", gstDecoder.ProcessLine, EndLine);
        return Ret;
    }

    /* top edge로 부터 둥그렇게 깎인 면은 건너뜀 */
    if ((gstDecoder.ProcessLine <= (gstDecoder.stEdgeTopLeft.Y + ROUND_HEIGHT)) ||
        (gstDecoder.ProcessLine <= (gstDecoder.stEdgeTopRight.Y + ROUND_HEIGHT)))
    {
        goto EXIT;
    }

    pLineLeftDot   = ImgSeekLine(gstDecoder.ProcessLine);
    pLineRightDot  = ImgSeekLine(gstDecoder.ProcessLine + 1) - gstDecoder.NumImg;

    DotOffset      = 0;
    SearchCnt      = 0;
    SearchLimit    = gstDecoder.NumDotPerRow / 2;  // two side search
    FoundLeft      = false;
    FoundRight     = false;

    /* find left and rigth edge */
    while (SearchCnt < SearchLimit)
    {
        if (*(pLineLeftDot + (DotOffset * gstDecoder.NumImg)) > PIXEL_THRESHOLD)
        {
            /* 가장 왼쪽에서 검출된 매체를 left side edge로 판단 */
            if (gstDecoder.stEdgeTopLeft.X > DotOffset)
            {
                gstDecoder.stEdgeTopLeft.X = DotOffset;

                FoundLeft = true;

                if (FoundRight == true)
                {
                    DBG_PRINT("found left edge position X : %d", gstDecoder.stEdgeTopLeft.X);
                    DBG_PRINT("found right edge position X : %d", gstDecoder.stEdgeTopRight.X);
                    DBG_PRINT("@ position Y :%d", gstDecoder.ProcessLine);

                    Ret = DECODER_RET_SUCCESS;
                    break;
                }
            }
        }

        if (*(pLineRightDot - (DotOffset * gstDecoder.NumImg)) > PIXEL_THRESHOLD)
        {
            /* 가장 오른쪽에서 검출된 매체를 right side edgh로 판단 */
            if ((gstDecoder.stEdgeTopRight.X == AXIS_NULL) ||
                (gstDecoder.stEdgeTopRight.X < (gstDecoder.NumDotPerRow - DotOffset)))
            {
                gstDecoder.stEdgeTopRight.X = gstDecoder.NumDotPerRow - DotOffset;

                FoundRight = true;

                if (FoundLeft == true)
                {
                    DBG_PRINT("found left edge position X : %d", gstDecoder.stEdgeTopLeft.X);
                    DBG_PRINT("found right edge position X : %d", gstDecoder.stEdgeTopRight.X);
                    DBG_PRINT("@ position Y :%d", gstDecoder.ProcessLine);

                    Ret = DECODER_RET_SUCCESS;
                    break;
                }
            }
        }

        DotOffset++;
        SearchCnt++;
    }

EXIT:
    gstDecoder.ProcessLine += PROCESS_Y_DEGREE;

    return Ret;
}

int DecoderFindDirection(unsigned int EndLine)
{
    unsigned char   *pLeftDot;
    unsigned char   *pRightDot;
    unsigned int    SearchCnt;
    unsigned int    LeftDotSum;
    unsigned int    RightDotSum;
    int             Ret;

    Ret = DECODER_RET_WAIT;

    if (EndLine < gstDecoder.ProcessLine + 1)
    {
        DBG_PRINT("%d/%d", gstDecoder.ProcessLine, EndLine);
        return Ret;
    }

    /* left side, righ side 각각 스캔 시작 */
    pLeftDot  = ImgSeekLine(gstDecoder.ProcessLine) + (gstDecoder.stEdgeTopLeft.X * gstDecoder.NumImg);
    pRightDot = ImgSeekLine(gstDecoder.ProcessLine) + (gstDecoder.stEdgeTopRight.X * gstDecoder.NumImg);

    LeftDotSum  = 0;
    RightDotSum = 0;

    /* 레퍼런스 마커 스캔 */
    for (SearchCnt = 0; SearchCnt < NUM_DOT_REF_MARK_WIDTH; SearchCnt++)
    {
        if (*pLeftDot <= PIXEL_THRESHOLD)
        {
            LeftDotSum++;
            if (LeftDotSum >= NUM_DOT_REF_MARK_DECISION)
            {
                if (RightDotSum > (NUM_DOT_REF_MARK_DECISION / 2))
                {
                    DBG_PRINT("detected ref marker on left side but right too %d", RightDotSum);
                    Ret = DECODER_RET_FAILURE;
                    break;
                }

                gstDecoder.Dir = MEDIA_DIR_FORWARD;
                DBG_PRINT("forward (X %d / Y %d)", gstDecoder.stEdgeTopLeft.X + SearchCnt, gstDecoder.ProcessLine);

                Ret = DECODER_RET_SUCCESS;
                break;
            }
        }

        if (*pRightDot <= PIXEL_THRESHOLD)
        {
            RightDotSum++;
            if (RightDotSum >= NUM_DOT_REF_MARK_DECISION)
            {
                if (LeftDotSum > (NUM_DOT_REF_MARK_DECISION / 2))
                {
                    DBG_PRINT("detected ref marker on right side but left too %d", LeftDotSum);
                    Ret = DECODER_RET_FAILURE;
                    break;
                }

                gstDecoder.Dir = MEDIA_DIR_BACKWARD;
                DBG_PRINT("backward (X %d / Y %d)", gstDecoder.stEdgeTopRight.X - SearchCnt, gstDecoder.ProcessLine);

                Ret = DECODER_RET_SUCCESS;
                break;
            }
        }

        pLeftDot  += gstDecoder.NumImg;
        pRightDot -= gstDecoder.NumImg;
    }

    gstDecoder.ProcessLine += PROCESS_Y_DEGREE;

    return Ret;
}

int DecoderFindRefMark(unsigned int EndLine)
{
    unsigned int  SearchCnt;
    unsigned char *pDot;
    unsigned int  DotSum;
    int           Ret;

    Ret = DECODER_RET_WAIT;

    if (EndLine < gstDecoder.ProcessLine + 1)
    {
        DBG_PRINT("%d/%d", gstDecoder.ProcessLine, EndLine);
        return Ret;
    }

    /* 단방향 스캔, 매체 방향에 따른 스캔 시작 픽셀 지정 */
    if (gstDecoder.Dir == MEDIA_DIR_FORWARD)
    {
        pDot = ImgSeekLine(gstDecoder.ProcessLine) + (gstDecoder.stEdgeTopLeft.X * gstDecoder.NumImg);
    }
    else if (gstDecoder.Dir == MEDIA_DIR_BACKWARD)
    {
        pDot = ImgSeekLine(gstDecoder.ProcessLine) + ((gstDecoder.stEdgeTopRight.X - (7 * PIXEL_PER_MM)) * gstDecoder.NumImg);
    }
    else
    {
        return DECODER_RET_FAILURE;
    }

    DotSum  = 0;

    for (SearchCnt = 0; SearchCnt < (7 * PIXEL_PER_MM); SearchCnt++)    // reference marker width = 7mm
    {
        if (*pDot <= PIXEL_THRESHOLD)
        {
            DotSum++;
            if (DotSum >= NUM_DOT_REF_MARK_DECISION)
            {
                //DBG_PRINT("%d #%d", gstDecoder.ProcessLine, gstDecoder.RefMarkIdx);

                gstDecoder.RefMarkYPos = gstDecoder.ProcessLine;
                Ret = DECODER_RET_SUCCESS;

                break;
            }
        }

        pDot += gstDecoder.NumImg;
    }

    gstDecoder.ProcessLine += PROCESS_Y_DEGREE;

    return Ret;
}

int DecoderFindUserMark(unsigned int EndLine)
{
    unsigned char   *pDot;
    unsigned int    MarkNum;
    unsigned int    DotOffset;
    int             Ret;

    Ret = DECODER_RET_WAIT;

    if (EndLine < gstDecoder.ProcessLine + 1)
    {
        return Ret;
    }

    /* 레퍼런스 마커에서 일정 거리 떨어져있는 유저마커 라인에 도달하지 않았을 때 */
    if (gstDecoder.RefMarkYPos + NUM_DOT_REF_USER_DISTANCE >= gstDecoder.ProcessLine)
    {
        goto EXIT;
    }

    pDot = ImgSeekLine(gstDecoder.ProcessLine) +
           ((gstDecoder.stEdgeTopLeft.X + MARKER_1ST_POSITION) * gstDecoder.NumImg);

    for (MarkNum = 0; MarkNum < MAX_SUPPORT_MARK_PER_ROW; MarkNum++)
    {
        for (DotOffset = 0; DotOffset < NUM_DOT_USER_MARK_WIDTH; DotOffset++)
        {
            if (*(pDot + (DotOffset * gstDecoder.NumImg)) < PIXEL_THRESHOLD)
            {
                gstDecoder.MarkList[gstDecoder.RefMarkIdx][MarkNum]++;
            }
        }

        if (gstDecoder.MarkList[gstDecoder.RefMarkIdx][MarkNum] > 0)
        {
            if (gstDecoder.Dir == MEDIA_DIR_FORWARD)
            {
                gstDecoder.MarkBitMap[MAX_SUPPORT_MARK_PER_COL - 1 - gstDecoder.RefMarkIdx] |= 1 << MarkNum;
            }
            else
            {
                gstDecoder.MarkBitMap[gstDecoder.RefMarkIdx] |= 1 << (MAX_SUPPORT_MARK_PER_ROW - MarkNum - 1);
            }
        }

        pDot += (NUM_DOT_USER_MARK_DISTANCE * gstDecoder.NumImg);
    }

EXIT:
    gstDecoder.ProcessLine += PROCESS_Y_DEGREE;
    if (gstDecoder.ProcessLine >= (gstDecoder.RefMarkYPos +
                                   NUM_DOT_REF_USER_DISTANCE +
                                   NUM_DOT_USER_MARK_HEIGHT))
    {
        gstDecoder.RefMarkIdx++;
        if (gstDecoder.RefMarkIdx == MAX_SUPPORT_MARK_PER_COL)
        {
            DBG_PRINT("user mark scan end");
            Ret = DECODER_RET_SUCCESS;
        }
        else
        {
            //DBG_PRINT("user mark line %d", gstDecoder.RefMarkIdx);
            Ret = DECODER_RET_OUTSTANDING;
        }
    }
    else
    {
        Ret = DECODER_RET_INPOGRESS;
    }

    return Ret;
}

unsigned char *GetOMRData(void)
{
    return (unsigned char *)gstDecoder.MarkBitMap;
}