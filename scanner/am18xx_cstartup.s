;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Part one of the system initialization code,
;; contains low-level
;; initialization.
;;
;; Copyright 2010 IAR Systems. All rights reserved.
;;
;; $Revision: 41301 $
;;

		MODULE  ?cstartup
AIC_HEADER		DEFINE	0x41504954
CMD_SEC_LOAD		DEFINE	0x58535901
CMD_SEC_FILL		DEFINE	0x5853590A
CMD_CRC_ENA		DEFINE	0x58535903
CMD_CRC_DIS	DEFINE	0x58535904
CMD_CRC_VAL	DEFINE	0x58535902
CMD_JMP_CLOSE	DEFINE	0x58535906
CMD_JMP		DEFINE	0x58535905
CMD_SEQ_READ	DEFINE	0x58535963
CMD_FINC_EXE	DEFINE	0x5853590D
CMD_SET		DEFINE	0x58535907

FUNC_PPL0		DEFINE	0x00020000
FUNC_PPL1		DEFINE	0x00020001
FUNC_CLK		DEFINE	0x00010002
FUNC_DDR		DEFINE	0x00080003
FUNC_SDRAM		DEFINE	0x00050004
FUNC_ASYNCH	DEFINE	0x00050005
FUNC_PPL0_CLK	DEFINE	0x00030006
FUNC_PSC		DEFINE	0x00010007
FUNC_PINMUX	DEFINE	0x00030008

		;; Forward declaration of sections.
		SECTION IRQ_STACK:DATA:NOROOT(3)
		SECTION FIQ_STACK:DATA:NOROOT(3)
		SECTION SVC_STACK:DATA:NOROOT(3)
		SECTION ABT_STACK:DATA:NOROOT(3)
		SECTION UND_STACK:DATA:NOROOT(3)
		SECTION CSTACK:DATA:NOROOT(3)


; --------------------
; Mode, correspords to bits 0-5 in CPSR

MODE_MSK DEFINE 0x1F			; Bit mask for mode bits in CPSR

USR_MODE DEFINE 0x10			; User mode
FIQ_MODE DEFINE 0x11			; Fast Interrupt Request mode
IRQ_MODE DEFINE 0x12			; Interrupt Request mode
SVC_MODE DEFINE 0x13			; Supervisor mode
ABT_MODE DEFINE 0x17			; Abort mode
UND_MODE DEFINE 0x1B			; Undefined Instruction mode
SYS_MODE DEFINE 0x1F			; System mode


CP_DIS_MASK		DEFINE  0xFFFFEFFA

/* This section should be at the begin of the code */
		SECTION .startup:CODE:NOROOT(2)

		EXTERN  ?main
		REQUIRE __vector

		ARM

#ifdef __NOR_BOOT__
; ---------------------------------------------------------
; BOOT Configuration Word
; ---------------------------------------------------------
ACCESS8BIT		DEFINE	0x0
ACCESS16BIT	DEFINE	0x1
NOR_LEGACY_BOOT   DEFINE	0x0
NOR_DIRECT_BOOT   DEFINE	0x10
NOR_AIS_BOOT	DEFINE	0x20
COPY1K			DEFINE	0x000
COPY2K			DEFINE	0x100
COPY3K			DEFINE	0x200
COPY4K			DEFINE	0x300
COPY5K			DEFINE	0x400
COPY6K			DEFINE	0x500
COPY7K			DEFINE	0x600
COPY8K			DEFINE	0x700
COPY9K			DEFINE	0x800
COPY10K		DEFINE	0x900
COPY11K		DEFINE	0xA00
COPY12K		DEFINE	0xB00
COPY13K		DEFINE	0xC00
COPY14K		DEFINE	0xD00
COPY15K		DEFINE	0xE00
COPY16K		DEFINE	0xF00

		DCD	NOR_DIRECT_BOOT | ACCESS16BIT ; Direct Mode Boot 16 Bit access
#endif

#if defined(__SPI_BOOT__) || defined(__NAND_BOOT__) || defined(__AIS_NOR_BOOT__)
	DCD   AIC_HEADER
#ifdef __SPI_BOOT__
	DCD   CMD_SEQ_READ	/* enable sequentiol read (this should speed up boot process)*/
	DCD   CMD_FINC_EXE	/* execute ROM function */
	DCD   FUNC_PPL0_CLK   /* initilaize PLL0 and SPI clock - the function expext 3 arguments*/
	DCD   0x00180101	/* PLL0 - CLKMODE, PLLM, PREDIV, POSTDIV */
	DCD   0x00000205	/* PLL0 - PLLDIV1, PLLDIV3, PLLDIV7 */
	DCD   0x0000000E	/* SPI PRESCALE */
	DCD   CMD_FINC_EXE	/* execute ROM function */
	DCD   FUNC_SDRAM	/* initilaize DDR and PLL1 clock - the function expext 8 arguments*/
	DCD   0xc0004522	/* SDCR */
	DCD   0x6e679a60	/* SDTIMR */
	DCD   0x00000008	/* SDSRETR */
	DCD   0x0000100e	/* SDRCR */
	DCD   0x00000000	/* DIV4p5_CLK_EN */
#endif
#ifdef __NAND_BOOT__
	DCD   CMD_FINC_EXE	/* execute ROM function */
	DCD   FUNC_PPL0	/* initilaize PLL0 2 arguments*/
	DCD   0x00180001	/* PLL0 - CLKMODE, PLLM, PREDIV, POSTDIV */
	DCD   0x00000B05	/* PLL0 - PLLDIV1, PLLDIV3, PLLDIV7 */
	DCD   CMD_FINC_EXE	/* execute ROM function */
	DCD   FUNC_ASYNCH	/* initilaize ASYNC memory - the function expext 5 arguments*/
	DCD   0x3FFFFFFC	/* CE2CFG */
	DCD   0x3FFFFFFC	/* CE3CFG */
	DCD   0x3FFFFFFC	/* CE4CFG */
	DCD   0x3FFFFFFC	/* CE5CFG */
	DCD   0x00000212	/* NANDFCR */
	DCD   CMD_FINC_EXE	/* execute ROM function */
	DCD   FUNC_DDR		/* initilaize DDR and PLL1 clock - the function expext 8 arguments*/
	DCD   0x18010001	/* PLL1 - PLLM, POSTDIV, PLLDIV1, PLLDIV1 */
	DCD   0x00000002	/* PLL1 - PLLDIV3 */
	DCD   0x000000C4	/* DDR DRPYC1R */
	DCD   0x0A034622	/* DDR SDCR */
	DCD   0x1E923249	/* DDR SDTIMR1 */
	DCD   0x4532A722	/* DDR SDTIMR2 */
	DCD   0x0000001F	/* DDR SDRCR */
	DCD   0x00000200	/* DDR PASR, ROWSIZE, CLK2XSRC */
#endif

/* Be careful, these definitions must be the last*/
	DCD   CMD_SEC_LOAD
	DCD   ?cstartup
	DCD   ?image_end - ?cstartup
#endif
__iar_program_start:
?cstartup:

; Execution starts here.
; After a reset, the mode is ARM, Supervisor, interrupts disabled.

// Disable Addr translation, D cache and enable I cache
		MRC		p15,0,R1,C1,C0,0
		LDR		R0,=CP_DIS_MASK	;; 0xFFFFEFFA
		AND		R1,R1,R0
		ORR		R1,R1,#(1<<12)
		MCR		p15,0,R1,C1,C0,0


; Initialize the stack pointers.
; The pattern below can be used for any of the exception stacks:
; FIQ, IRQ, SVC, ABT, UND, SYS.
; The USR mode uses the same stack as SYS.
; The stack segments must be defined in the linker command file,
; and be declared above.
		mrs	r0,cpsr								; Original PSR value
		bic	r0,r0,#MODE_MSK					; Clear the mode bits
		orr	r0,r0,#SVC_MODE					; Set Supervisor mode bits
		msr	cpsr_c,r0							; Change the mode
		ldr	sp,=SFE(SVC_STACK)				; End of SVC_STACK

		bic	r0,r0,#MODE_MSK					; Clear the mode bits
		orr	r0,r0,#ABT_MODE					; Set Abort mode bits
		msr	cpsr_c,r0							; Change the mode
		ldr	sp,=SFE(ABT_STACK)				; End of ABT_STACK

		bic	r0,r0,#MODE_MSK					; Clear the mode bits
		orr	r0,r0,#UND_MODE					; Set Undefined mode bits
		msr	cpsr_c,r0							; Change the mode
		ldr	sp,=SFE(UND_STACK)				; End of UND_STACK

		bic	r0,r0,#MODE_MSK					; Clear the mode bits
		orr	r0,r0,#FIQ_MODE					; Set FIR mode bits
		msr	cpsr_c,r0							; Change the mode
		ldr	sp,=SFE(FIQ_STACK)				; End of FIQ_STACK

		bic	r0,r0,#MODE_MSK					; Clear the mode bits
		orr	r0,r0,#IRQ_MODE					; Set IRQ mode bits
		msr	cpsr_c,r0							; Change the mode
		ldr	sp,=SFE(IRQ_STACK)				; End of IRQ_STACK

		bic	r0,r0,#MODE_MSK					; Clear the mode bits
		orr	r0,r0,#SYS_MODE					; Set System mode bits
		msr	cpsr_c,r0							; Change the mode
		ldr	sp,=SFE(CSTACK)					; End of CSTACK

#ifdef __ARMVFP__
; Enable the VFP coprocessor.
		mov	r0, #BASE_ARD_EIM				; Set EN bit in VFP
		fmxr	fpexc, r0							; FPEXC, clear others.

; Disable underflow exceptions by setting flush to zero mode.
; For full IEEE 754 underflow compliance this code should be removed
; and the appropriate exception handler installed.
		mov	r0, #0x01000000								; Set FZ bit in VFP
		fmxr	fpscr, r0							; FPSCR, clear others.
#endif

; Add more initialization here

; Continue to ?main for more IAR specific system startup
		ldr	r0,=?main
		bx	r0

;
; The module in this file are included in the libraries, and may be
; replaced by any user-defined modules that define the PUBLIC symbol
; __iar_program_start or a user defined start symbol.
;
; To override the cstartup defined in the library, simply add your
; modified version to the workbench project.

	SECTION .intvec:CODE:NOROOT(2)

		PUBLIC  __vector
		PUBLIC  __iar_program_start
		EXTERN  Undefined_Handler
		EXTERN  SWI_Handler
		EXTERN  Prefetch_Handler
		EXTERN  Abort_Handler
		EXTERN  IRQ_Handler
		EXTERN  FIQ_Handler

		ARM

__vector:
		; All default exception handlers (except reset) are
		; defined as weak symbol definitions.
		; If a handler is defined by the application it will take precedence.
		LDR	PC,Reset_Addr		; Reset
		LDR	PC,Undefined_Addr	; Undefined instructions
		LDR	PC,SWI_Addr			; Software interrupt (SWI/SVC)
		LDR	PC,Prefetch_Addr		; Prefetch abort
		LDR	PC,Abort_Addr		; Data abort
		DCD	0						; RESERVED
		LDR	PC,IRQ_Addr			; IRQ
		LDR	PC,FIQ_Addr			; FIQ

		DATA

Reset_Addr:	DCD   __iar_program_start
Undefined_Addr: DCD   Undefined_Handler
SWI_Addr:	DCD   SWI_Handler
Prefetch_Addr:  DCD   Prefetch_Handler
Abort_Addr:	DCD   Abort_Handler
IRQ_Addr:	DCD   IRQ_Handler
__vector_end:
FIQ_Addr:	DCD   FIQ_Handler

/* Be careful, this section should be the end of the code */
#if defined(__SPI_BOOT__) || defined(__NAND_BOOT__) || defined(__AIS_NOR_BOOT__)
		SECTION .image_end:CODE:ROOT(2)
?image_end:
		DCD	CMD_JMP_CLOSE
		DCD	?cstartup
#else
		SECTION .image_end:CODE:ROOT(2)
?image_end:
		DCD	CMD_JMP_CLOSE
		DCD	?cstartup   
#endif

				END
