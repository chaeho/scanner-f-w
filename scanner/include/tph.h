#ifndef __PRN_TPH_H__
#define __PRN_TPH_H__


/* return type definition */
#define TPH_RET_SUCCESS                 (0)
#define TPH_RET_WAIT                    (1)
#define TPH_RET_FAILURE                 (-1)

#define TPH_DATA_CHUNK_LEN              (256)

#define BIT_PER_BYTE                    (8)
#define DOT_PER_LOW                     (256)   // tph의 실제도트 개수
#define DOT_PER_COLUM                   (256)   // 인쇄할수 있는 최대 라인 수
#define DOT_PER_MM                      (8)
#define BYTE_PER_LOW                    (DOT_PER_LOW / BIT_PER_BYTE)

#define TPH_BYTE_BUF_LENGTH			    (DOT_PER_LOW * DOT_PER_COLUM)
#define TPH_BIT_BUF_LENGTH              (TPH_BYTE_BUF_LENGTH / BIT_PER_BYTE)


typedef enum TPHSTATE
{
	TPH_IDLE,
	TPH_START,
	TPH_RUN,
	TPH_DONE
}   TPHSTATE;


unsigned short TPHInit(void);

void BranderHomePosition(void);
void BranderTphPosition(void);

int TPHGetImageInfo(unsigned short ImageSize);
int TPHGetImageData(unsigned char *pImage);

void TPHBrand(void);

#endif