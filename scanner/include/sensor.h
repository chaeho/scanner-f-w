#ifndef __SESNSOR_H__
#define __SESNSOR_H__


/* return type definition */
#define SENS_RET_SUCCESS                    (0)
#define SENS_RET_DETECT                     (1)
#define SENS_RET_DOUBLE                     (2)
#define SENS_RET_NOTDETECT                  (4)
#define SENS_RET_COVER_OPEN                 (5)

/* sensor channel definition */
#define SENS_NUM_CHANNEL	                (16)
#define SENS_NUM_RESERVED_CHANNEL           (7)
#define SENS_NUM_AVAILABLE_CHANNEL          (9)
#define SENS_MASK_ALL                       ((1 << SENS_ENTRANCE_1) | \
                                             (1 << SENS_ENTRANCE_2) | \
                                             (1 << SENS_EXIT)       | \
                                             (1 << SENS_ENTRANCE_3) | \
                                             (1 << SENS_COVER)      | \
                                             (1 << SENS_ENTRANCE_4) | \
                                             (1 << SENS_TPH))
#define SENS_MASK_PAPER_IN                  ((1 << SENS_ENTRANCE_1) | \
                                             (1 << SENS_ENTRANCE_2) | \
                                             (1 << SENS_ENTRANCE_3) | \
                                             (1 << SENS_ENTRANCE_4))

/* paper location detect type */
#define SENSE_IN_PAPER_ON					(0)
#define SENSE_IN_PAPER_OFF					(1)
#define SENSE_OUT_TPH_ON					(2)
#define SENSE_IN_TPH_ON						(5)
#define SENSE_IN_TPH_OFF					(6)
#define SENSE_OUT_TPH_OFF					(7)
#define SENSE_EJECT_1_ON					(8)
#define SENSE_EJECT_1_OFF					(9)

#define REGISTER_NUM_CONTENTS_BYTE			(16)
#define REGISTER_NUM_RSV_BYTE				(14)
#define REGISTER_NUM_CHECK_SUM_BYTE			(2)


typedef enum sensor_e
{
    SENS_ENTRANCE_1     = 0,
    SENS_ENTRANCE_2     = 1,
    SENS_EXIT           = 2,
    SENS_ENTRANCE_3     = 8,
    SENS_COVER          = 9,
    SENS_ENTRANCE_4     = 10,
    SENS_TPH            = 11,
    SENS_DOUBLE_MEDIA   = 12, // 논리적 채널, 실제로는 thermistor 에 연결되어 있으나 안씀
}   SENS;


typedef struct register_t
{
    unsigned char TopMargin;
    unsigned char BottomMargin;
    unsigned char entranceON;
    unsigned char entranceOFF;
    unsigned char exitON;
    unsigned char exitOFF;
    unsigned char injectON;
    unsigned char injectOFF;
    unsigned char bayFull;
    unsigned char upCover;
    unsigned char homeON;
    unsigned char homeOFF;
    unsigned char doubleMedia;
    unsigned char brandCheck;
    unsigned char DACR;
    unsigned char gain;
    unsigned char reserved[REGISTER_NUM_RSV_BYTE];
    unsigned char checkSum[REGISTER_NUM_CHECK_SUM_BYTE];    
}   REGISTER;


unsigned short MaskSensorON(unsigned short MaskBitmap);
unsigned short MaskSensorOFF(unsigned short MaskBitmap);
unsigned int CheckPaperLocation(unsigned char Location);
int ReadSensor(unsigned short ReadSensBitmap, unsigned char *SensVal);

void CheckParam(REGISTER *pstRegister);
void GetParam(REGISTER *pstRegister);
void SetParam(REGISTER *pstRegister);

void GetSensorAll(void);

#endif
