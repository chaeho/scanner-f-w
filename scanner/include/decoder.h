#ifndef __DECODER_H__
#define __DECODER_H__


/* return type */
#define DECODER_RET_SUCCESS             (0)
#define DECODER_RET_FAILURE             (-1)
#define DECODER_RET_WAIT                (1)
#define DECODER_RET_INPOGRESS           (2)
#define DECODER_RET_OUTSTANDING         (3)

/* configuration */
#define MAX_SUPPORT_MARK_PER_ROW        (14)
#define MAX_SUPPORT_MARK_PER_COL        (32)

#define OMR_DATA_LENGTH                 (64)


typedef struct _axis
{
    unsigned short  X;
    unsigned short  Y;
}   AXIS;

typedef struct _decoder
{
    unsigned short  MarkBitMap[MAX_SUPPORT_MARK_PER_COL];
    unsigned short  MarkList[MAX_SUPPORT_MARK_PER_COL][MAX_SUPPORT_MARK_PER_ROW];

    AXIS            stEdgeTopLeft;
    AXIS            stEdgeTopRight;

    unsigned short  RefMarkYPos; // [MAX_SUPPORT_MARK_PER_COL];
    unsigned int    RefMarkIdx;

    unsigned int    Dir;        // forward(0) or backward(1)
    unsigned int    ProcessLine;

    unsigned short  NumDotPerRow;
    unsigned short  NumImg;
    unsigned char   *pImgBuf;
}   DECODER;


void DecoderInit(unsigned char *pImgBuf, unsigned short NumDotPerRow, unsigned short NumImgPerBuf);
int DecoderFindTopEdge(unsigned int EndLine);
int DecoderFindLeftRightEdge(unsigned int EndLine);
int DecoderFindDirection(unsigned int EndLine);
int DecoderFindRefMark(unsigned int EndLine);
int DecoderFindUserMark(unsigned int EndLine);
unsigned char *GetOMRData(void);

#endif
