#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__


#include "devcfg.h"


/* return type definition */
#define CTRL_RET_SUCCESS                (0)
#define CTRL_RET_FAILURE                (1)
#define CTRL_RET_FORWARD_TIMEOUT        (-1)
#define CTRL_RET_BACKWARD_TIMEOUT       (-2)


/* device state definition */
#define DEVICE_STATE_IDLE				(0)
#define DEVICE_STATE_IDENT				(1)
#define DEVICE_STATE_STANDBY			(2)
#define DEVICE_STATE_SCAN				(3)
#define DEVICE_STATE_LOAD				(4)
#define DEVICE_STATE_BRAND				(5)
#define DEVICE_STATE_CALIBRATION        (6)

/* result code definition @ response */
#define RESULT_RESERVED_0				(1 << 0)
#define RESULT_RESERVED_1				(1 << 1)
#define RESULT_RESERVED_2				(1 << 2)
#define RESULT_RESERVED_3		        (1 << 3)
#define RESULT_TIMEOUT					(1 << 4)
#define RESULT_INVAL_CMD				(1 << 5)
#define RESULT_SEQUENCE_VIOLATION       (1 << 6)
#define RESULT_BUSY                     (1 << 7)

/* scan state definition (scan, brand) */
#define OP_STATE_IDLE					(0)
#define OP_STATE_WAIT					(1)
#define OP_STATE_RUN					(2)
#define OP_STATE_DONE					(3)
#define OP_STATE_BUSY                   (4)

/* OMR decoding state definition */
#define OMR_STATE_IDLE                  (0)
#define OMR_STATE_TOP_MARGIN            (1)
#define OMR_STATE_SIDE_MARGIN           (2)
#define OMR_STATE_DIRECTION             (3)
#define OMR_STATE_REF_MARK              (4)
#define OMR_STATE_USR_MARK              (5)
#define OMR_STATE_DONE                  (6)

/* device status byte length */
#define DEVICE_STATUS_SENSOR_LENGTH		        (2)
#define DEVICE_STATUS_RESEVED2_LENGTH	        (2)
#define DEVICE_STATUS_SCANLINE_LENGTH	        (2)
#define DEVICE_STATUS_BRAND_IMAGE_SIZE_LENGTH	(4)

/* response packet field size */
#define RSP_PKT_HEADER_LENGTH           (8)
#define RSP_PKT_FW_VER_LENGTH			(4)
#define RSP_PKT_SN_LENGTH				(32)
#define RSP_PKT_STATUS_LENGTH			(16)


typedef struct status_t
{
	union
	{
		struct
		{
			unsigned char	    DevState;                                   // 8B
			unsigned char	    ScanState;                                  // 9B
			unsigned char	    BrandState;                                 // 10B
			unsigned char	    CalibState;                                 // 11
            union
            {
    			unsigned char	aSensor[DEVICE_STATUS_SENSOR_LENGTH];       // 12, 13B
                unsigned short  Sensor;
            };
			unsigned char       Motor;                                      // 14B
            unsigned char       DecodeState;                                // 15B
            unsigned char	    LED;                                        // 16B
            unsigned char       Reserved1;                                  // 17B
            union
            {
                unsigned char	aScanLine[DEVICE_STATUS_SCANLINE_LENGTH];   // 18, 19B
                unsigned short  NumScanLine;
            };
            union
            {
                unsigned char   aBrandImageSize[DEVICE_STATUS_BRAND_IMAGE_SIZE_LENGTH];  // 20, 21B
                unsigned short  BrandImageSize;
            };
            unsigned char       Reserved2[DEVICE_STATUS_RESEVED2_LENGTH];    // 23, 24B
		};
		unsigned char Status[RSP_PKT_STATUS_LENGTH];
	};
}   STATUS;


/* Interface */
void CTRLInit(void);
void CTRLRun(void);

void RunCalibration(void);

#endif
