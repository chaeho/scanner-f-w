#ifndef __CIS_H__
#define __CIS_H__


#define NUM_CIS_CHANNEL                     (2)
#define CIS_NUM_DOT_PER_SCAN                (832)
#define BYTE_PER_SCAN_TRANSACTION			(CIS_NUM_DOT_PER_SCAN * NUM_CIS_CHANNEL)

#define PIXEL_PER_MM				        (8)
#define MM_TO_PIXEL(MM)                     ((MM) * PIXEL_PER_MM)
#define MAX_PIXEL_PER_TICKET			    (1500)

/* pin number */
#define PIN_PLD_RESET			            (131)
#define PIN_PLD_SI							(26)
#define PIN_AFE_DOE							(130)
#define PIN_CIS_LED							(5)

#define MAX_IMG_LINE						(2500)

#define CPLD_PERIOD							(199)


void CISInit(void);

void CISDOutEnable(unsigned char *pImgBuf);
unsigned int CISDOutDisable(void);
unsigned short CISCheckLine(void);
void VPIFDMASetup(void);

void LEDTest(unsigned int ONOFF);
#endif
