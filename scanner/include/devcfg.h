#ifndef __DEVICE_CONFIG_H__
#define __DEVICE_CONFIG_H__


//======================================================================
/* Target ���� */
#define ENABLE_DEBUG					(true)

#define MOTOR_SPD_650MM                 (1)
#define MOTOR_SPD_730MM                 (2)
#define MOTOR_SPD_SELECT                (MOTOR_SPD_650MM)

#define DISTANCE_ENTRANCE_EXIT_SENS     (32)
#define DISTANCE_ROLLER_TO_ROLLER       (60)

#define LOCATE_PRINT_POS_Y              (45)
//======================================================================
/* Flash space layout */
#define FLASH_REGISTER_ADDR				(0x00360000)
#define FLASH_DEVICE_SN_ADDR			(0x00361000)
#define FALSH_BRAND_IMAGE_INFO_ADDR     (0x00362000)
#define FALSH_BRAND_IMAGE_DATA_ADDR     (0x00363000)    // 8KB

/* Firmware version */
#define FW_VER                          ("1201")

#define BRAND_IMAGE_SIZE_LEN            (2)


typedef enum command
{
	RESET			= 0x0000,
	GET_FW_VER		= 0x0001,
	GET_SN			= 0x0002,
	SET_SN			= 0x0003,
	CHECK_STATUS	= 0x0004,
	GET_PARAM		= 0x0100,
	SET_PARAM		= 0x0101,
	SOL_ON			= 0x0200,
	SOL_OFF			= 0x0201,
	MOTOR_ON		= 0x0202,
	MOTOR_OFF		= 0x0203,
	LED_CTRL		= 0x0204,
	SCAN_START		= 0x0300,
	STOP		    = 0x0301,
	GET_IMG			= 0x0302,
	GET_AXIS		= 0x0303,
	INJECT			= 0x0400,
	REJECT			= 0x0401,
	BRAND_INFO		= 0x0500,
    BRAND_DATA      = 0x0501,
    BRANDING        = 0x0502,
	CALIBLATION		= 0x0600,
	FW_UPDATE		= 0x0F0F,
	FACTORY_INIT	= 0xFFFF
}   CMD;

typedef enum SHADING
{
	IDLE,
	BLACKSHADING,
	DACR,
	GAINCAL,
	GAIN,
	PEAKCAL,
	PEAK,
}   SHADING;

#endif