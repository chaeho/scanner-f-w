#ifndef __ACTUATOR_H__
#define __ACTUATOR_H__

#define MOTOR_CNT							(1)

#define MOTOR_TYPE_MAIN						(0)
#define MOTOR_TYPE_BRANDAR					(1)

#define MOTOR_DIRECTION_FORWARD				(0)
#define MOTOR_DIRECTION_BACKWARD			(1)

#define SOL_TYPE_1							(1 << 0)
#define SOL_TYPE_2							(1 << 1)

/* Actuator pin number */
#define PIN_SOL_INJECT						(8)
#define PIN_SOL_REJECT						(1)
#define PIN_MAIN_MOT_EN						(21)
#define PIN_MAIN_MOT_DIR					(95)
#define PIN_MAIN_MOT_STEP					(96)
#define PIN_MAIN_MOT_SPD_SEL_1			    (65)
#define PIN_MAIN_MOT_SPD_SEL_2			    (66)
#define PIN_UP_COVER						(7)

#define MOTOR_FACT							(24)

/* motor speed mode definition */
#define MOTOR_SPD_MODE_NONE					(0)
#define MOTOR_SPD_MODE_NORMAL				(1)
#define MOTOR_SPD_MODE_BRAND				(2)

#define MOTOR_TIMEOUT_MS                    (1500)

#define MM_TO_STEP(X)                       ((X) * 5)   // 1 tick = 0.21mm


void ActuatorInit(void);

void MotorStart(unsigned int SpdMode, unsigned int Dir);
void MotorEnd(void);

unsigned int GetMotorTick(void);

void MotorONTest(unsigned int MotorType, unsigned int Direction);
void MotorOFFTest(unsigned int MotorType);

#endif
