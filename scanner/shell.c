#include <string.h>
#include <stdio.h>

#include "sensor.h"
#include "tph.h"
#include "controller.h"

#include "uart.h"


#define INC_SET(X, Y)                   ((X) = (((X) + 1) & ((Y) - 1)))

#define CONSOL_BUF_LENGTH               (16)

#define PROCESS_PERIODE                 (20000)

#define PROCESS_ID_NONE                 (0)
#define PROCESS_ID_SENSOR               (1)


char            gaConsoleBuf[CONSOL_BUF_LENGTH];
unsigned int    Idx;
unsigned int    gProcID;
unsigned int    gTick;


void CMDLauncher(void);
void ProcessScheduler(void);


void RunShell(void)
{
    CMDLauncher();
    ProcessScheduler();
}


void CMDLauncher(void)
{
    int     c;
    char    *pToken;

    c = UARTrxGetChar();
    if (c != -1)
    {
        if (c == '\r')
        {
            printf("\r\n");
            gaConsoleBuf[Idx] = 0;
            Idx = 0;

            if (strcmp("help", gaConsoleBuf) == 0)
            {
                printf("sensor [on/off]\r\n");
                printf("tph [on/off]\r\n");
            }
            else
            {
                pToken = strtok(gaConsoleBuf, " ");

                if (strcmp("sensor", pToken) == 0)
                {
                    pToken = strtok(NULL, " ");
                    if (strcmp("on", pToken) == 0)
                    {
                        gProcID = PROCESS_ID_SENSOR;
                    }
                    else if (strcmp("off", pToken) == 0)
                    {
                        gProcID = PROCESS_ID_NONE;
                    }
                }
                else if (strcmp("tph", pToken) == 0)
                {
                    pToken = strtok(NULL, " ");
                    if (strcmp("on", pToken) == 0)
                    {
                        BranderTphPosition();
                    }
                    else if (strcmp("off", pToken) == 0)
                    {
                        BranderHomePosition();
                    }
                }
                else
                {
                    printf("invalid command %s\r\n", gaConsoleBuf);
                    Idx = 0;
                }
            }
        }
        else if (c == '\b')
        {
            if (Idx > 0)
            {
                Idx--;
                printf(" \b");
            }
        }
        else
        {
            gaConsoleBuf[Idx] = c;
            INC_SET(Idx, CONSOL_BUF_LENGTH);
        }
    }
}

void ProcessScheduler(void)
{
    if (gTick == PROCESS_PERIODE)
    {
        switch (gProcID)
        {
        case PROCESS_ID_SENSOR:
            GetSensorAll();
            break;

        case PROCESS_ID_NONE:
        default:
            break;
        }

        gTick = 0;
    }

    gTick++;
}