#include <string.h>

#include "gpio.h"
#include "soc_AM1808.h"

#include "spi_io.h"
#include "flash.h"
#include "sensor.h"

#include "debug.h"


#define ADC_ADDR_CONVERT_SPI(ADDR)          (0x1800 | ((ADDR) << 7))
#define SPI_VAL_CONVERT_ADC(VAL)            (((VAL) & 0x00000FF0) >> 4)


const REGISTER          gstRegInit =
{
    .TopMargin      = 0,
    .BottomMargin   = 0,
    .entranceON     = 60,
    .entranceOFF    = 30,
    .exitON         = 80,
    .exitOFF        = 200,
    .injectON       = 220,
    .injectOFF      = 220,
    .bayFull        = 180,
    .upCover        = 120,
    .homeON         = 240,
    .homeOFF        = 15,
    .doubleMedia    = 180,
    .brandCheck     = 85,
    .DACR           = 0,
    .gain           = 40,
    .reserved       = {0, },
    .checkSum       = {0, }
};
REGISTER                gstReg;


/* local function declalation */
void LoadParam(REGISTER *pstRegister);
void StoreParam(REGISTER *pstRegister);


/* local function definition */
unsigned short MaskSensorON(unsigned short MaskBitmap)
{
    unsigned short	OnSensor;
    unsigned char	SensVal[SENS_NUM_CHANNEL];

    if ((MaskBitmap == 0) ||
        ((MaskBitmap & ~SENS_MASK_ALL) > 0))
    {
        return 0;
    }

    ReadSensor(MaskBitmap, SensVal);

    OnSensor = 0;

    if ((MaskBitmap & (1 << SENS_ENTRANCE_1)) &&
        (SensVal[SENS_ENTRANCE_1] > gstReg.entranceON))
    {
        OnSensor |= (1 << SENS_ENTRANCE_1);

        if (SensVal[SENS_ENTRANCE_1] > gstReg.doubleMedia)
        {
            OnSensor |= (1 << SENS_DOUBLE_MEDIA);
        }
    }

    if ((MaskBitmap & (1 << SENS_ENTRANCE_2)) &&
        (SensVal[SENS_ENTRANCE_2] > gstReg.entranceON))
    {
        OnSensor |= (1 << SENS_ENTRANCE_2);

        if (SensVal[SENS_ENTRANCE_2] > gstReg.doubleMedia)
        {
            OnSensor |= (1 << SENS_DOUBLE_MEDIA);
        }
    }

    if ((MaskBitmap & (1 << SENS_EXIT)) &&
        (SensVal[SENS_EXIT] < gstReg.exitON))      // 버스표의 검은색 박스가 걸쳤을 때의 센서 값
    {
        OnSensor |= (1 << SENS_EXIT);
    }

    if ((MaskBitmap & (1 << SENS_ENTRANCE_3)) &&
        (SensVal[SENS_ENTRANCE_3] > gstReg.entranceON))
    {
        OnSensor |= (1 << SENS_ENTRANCE_3);

        if (SensVal[SENS_ENTRANCE_3] > gstReg.doubleMedia)
        {
            OnSensor |= (1 << SENS_DOUBLE_MEDIA);
        }
    }

    if ((MaskBitmap & (1 << SENS_COVER)) &&
        (SensVal[SENS_COVER] > gstReg.bayFull))
    {
        OnSensor |= (1 << SENS_COVER);
    }

    if ((MaskBitmap & (1 << SENS_ENTRANCE_4)) &&
        (SensVal[SENS_ENTRANCE_4] > gstReg.entranceON))
    {
        OnSensor |= (1 << SENS_ENTRANCE_4);

        if (SensVal[SENS_ENTRANCE_4] > gstReg.doubleMedia)
        {
            OnSensor |= (1 << SENS_DOUBLE_MEDIA);
        }
    }

    if ((MaskBitmap & (1 << SENS_TPH)) &&
        (SensVal[SENS_TPH] > gstReg.homeON))
    {
        OnSensor |= (1 << SENS_TPH);
    }

    return OnSensor;
}

unsigned short MaskSensorOFF(unsigned short MaskBitmap)
{
    unsigned short	OnSensor;
    unsigned char	SensVal[SENS_NUM_CHANNEL];

    if ((MaskBitmap == 0) ||
        ((MaskBitmap & ~SENS_MASK_ALL) > 0))
    {
        return 0;
    }

    ReadSensor(MaskBitmap, SensVal);

    OnSensor = 0;

    if ((MaskBitmap & (1 << SENS_ENTRANCE_1)) &&
        (SensVal[SENS_ENTRANCE_1] < gstReg.entranceOFF))
    {
        OnSensor |= (1 << SENS_ENTRANCE_1);
    }

    if ((MaskBitmap & (1 << SENS_ENTRANCE_2)) &&
        (SensVal[SENS_ENTRANCE_2] < gstReg.entranceOFF))
    {
        OnSensor |= (1 << SENS_ENTRANCE_2);
    }

    if ((MaskBitmap & (1 << SENS_EXIT)) &&
        (SensVal[SENS_EXIT] > gstReg.exitOFF))  // 승차권의 검은색 박스가 걸치지 않았을때의 센서값
    {
        OnSensor |= (1 << SENS_EXIT);
    }

    if ((MaskBitmap & (1 << SENS_ENTRANCE_3)) &&
        (SensVal[SENS_ENTRANCE_3] < gstReg.entranceOFF))
    {
        OnSensor |= (1 << SENS_ENTRANCE_3);
    }

    if ((MaskBitmap & (1 << SENS_COVER)) &&
        (SensVal[SENS_COVER] < gstReg.bayFull))
    {
        OnSensor |= (1 << SENS_COVER);
    }

    if ((MaskBitmap & (1 << SENS_ENTRANCE_4)) &&
        (SensVal[SENS_ENTRANCE_4] < gstReg.entranceOFF))
    {
        OnSensor |= (1 << SENS_ENTRANCE_4);
    }

    if ((MaskBitmap & (1 << SENS_TPH)) &&
        (SensVal[SENS_TPH] < gstReg.homeOFF))
    {
        OnSensor |= (1 << SENS_TPH);
    }

    return OnSensor;
}

unsigned int CheckPaperLocation(unsigned char Location)
{
    unsigned int	Ret;
    unsigned char	SensVal[SENS_NUM_CHANNEL];

    /*
     * 용지 투입이 검출 되었다면 모터가 동작할것인데 모터 ISR에서 TPH in 센서 이후에 이동한
     * 용지의 거리를 계산하기 위해서는 이 루틴에서 TPH in 센서를 계속 읽어야 한다.
     */
    if (Location == SENSE_IN_PAPER_ON)
    {
        ReadSensor((1 << SENS_ENTRANCE_1) |
                   (1 << SENS_ENTRANCE_2) |
                   (1 << SENS_ENTRANCE_3) |
                   (1 << SENS_ENTRANCE_4) |
                   (1 << SENS_COVER),           SensVal);

        if (SensVal[SENS_COVER] > gstReg.upCover)
        {
            DBG_PRINT("cover open");
            return SENS_RET_COVER_OPEN;
        }

        if (((SensVal[SENS_ENTRANCE_1] > gstReg.doubleMedia) &&
             (SensVal[SENS_ENTRANCE_2] > gstReg.doubleMedia)) ||
            ((SensVal[SENS_ENTRANCE_2] > gstReg.doubleMedia) &&
             (SensVal[SENS_ENTRANCE_3] > gstReg.doubleMedia)) ||
            ((SensVal[SENS_ENTRANCE_3] > gstReg.doubleMedia) &&
             (SensVal[SENS_ENTRANCE_4] > gstReg.doubleMedia)))
        {
            DBG_PRINT("paper in detected double media");
            Ret = SENS_RET_DOUBLE;
        }
        else if ((SensVal[SENS_ENTRANCE_1] > gstReg.entranceON) ||
                 (SensVal[SENS_ENTRANCE_2] > gstReg.entranceON) ||
                 (SensVal[SENS_ENTRANCE_3] > gstReg.entranceON) ||
                 (SensVal[SENS_ENTRANCE_4] > gstReg.entranceON))
        {
            Ret = SENS_RET_SUCCESS;
        }
        else
        {
            Ret = SENS_RET_NOTDETECT;
        }
    }
    else  // SENSE_IN_PAPER_OFF
    {
        ReadSensor((1 << SENS_ENTRANCE_1) |
                   (1 << SENS_ENTRANCE_2) |
                   (1 << SENS_ENTRANCE_3) |
                   (1 << SENS_ENTRANCE_4) |
                   (1 << SENS_EXIT),            SensVal);

        if (((SensVal[SENS_ENTRANCE_1] > gstReg.doubleMedia) &&
             (SensVal[SENS_ENTRANCE_2] > gstReg.doubleMedia)) ||
            ((SensVal[SENS_ENTRANCE_2] > gstReg.doubleMedia) &&
             (SensVal[SENS_ENTRANCE_3] > gstReg.doubleMedia)) ||
            ((SensVal[SENS_ENTRANCE_3] > gstReg.doubleMedia) &&
             (SensVal[SENS_ENTRANCE_4] > gstReg.doubleMedia)))
        {
            DBG_PRINT("paper out detected double media");
            Ret = SENS_RET_DOUBLE;
        }
        else
        {
            if ((SensVal[SENS_ENTRANCE_1] > gstReg.entranceOFF) ||
                (SensVal[SENS_ENTRANCE_2] > gstReg.entranceOFF) ||
                (SensVal[SENS_ENTRANCE_3] > gstReg.entranceOFF) ||
                (SensVal[SENS_ENTRANCE_4] > gstReg.entranceOFF ))
            {
                Ret = SENS_RET_DETECT;
            }
            else
            {
                if (SensVal[SENS_EXIT] < gstReg.exitOFF)
                {
                    Ret = SENS_RET_DETECT;
                }
                else
                {
                    Ret = SENS_RET_SUCCESS;				// through out
                }
            }
        }
    }

    return Ret;
}

int ReadSensor(unsigned short ReadSensBitmap, unsigned char *SensVal)
{
    unsigned int SndPtr;
    unsigned int RcvPtr;
    unsigned int SndCnt;
    unsigned int SndData;
    unsigned int RcvData;

    if ((ReadSensBitmap == 0) ||
        (ReadSensBitmap & ~SENS_MASK_ALL) > 0) // invalid channel !! (3, 4, 5, 6, 7)
    {
        return -1;
    }

    ReadSensBitmap |= (1 << 14) | (1 << 15);	// dummy cycle 2 bitmap 포함

    SndCnt = 0;
    SndPtr = 0;
    RcvPtr = 0;

    while (SndPtr < SENS_NUM_CHANNEL)
    {
        if ((ReadSensBitmap & (1 << SndPtr)) > 0)
        {
            SndData = ADC_ADDR_CONVERT_SPI(SndPtr);

            RcvData = SPI0Transfer16b(SPI0_DEVICE_ID_ADC, SndData, SPI_IO_FLAG_RW);

            SndCnt++;
            SndPtr++;

            if (SndCnt > 2)
            {
                while (RcvPtr < (SENS_NUM_CHANNEL - 2))
                {
                    if ((ReadSensBitmap & (1 << RcvPtr)) > 0)
                    {
                        SensVal[RcvPtr] = (unsigned char)SPI_VAL_CONVERT_ADC(RcvData);
                        RcvPtr++;
                        break;
                    }
                    else
                    {
                        RcvPtr++;
                    }
                }
            }
        }
        else
        {
            SndPtr++;
        }
    }

    return 0;
}

void CheckParam(REGISTER *pstRegister)
{
    unsigned short	i, sum, checksum;
    unsigned char   *p;

    p = (unsigned char *)pstRegister;
    for (i = sum = 0; i < sizeof(REGISTER) - REGISTER_NUM_CHECK_SUM_BYTE; i++)
    {
        sum += (p[i] & 0xff);
    }

    checksum = p[i] | (p[i + 1] << 8);
    if (sum != checksum)
    {
        DBG_PRINT("restore register");

        memcpy(pstRegister, (unsigned char *)&gstRegInit, sizeof(REGISTER));
        StoreParam(pstRegister);
    }
}

void LoadParam(REGISTER *pstRegister)
{
    FlashRead(FLASH_REGISTER_ADDR, (unsigned char *)pstRegister, sizeof(REGISTER));
}

void StoreParam(REGISTER *pstRegister)
{
    unsigned short	i, sum;
    unsigned char *p;

    p = (unsigned char *)pstRegister;
    for (i = sum = 0; i < sizeof(REGISTER) - REGISTER_NUM_CHECK_SUM_BYTE; i++)
    {
        sum += (p[i] & 0xff);
    }

    p[i] = sum & 0xff;
    p[i + 1] = sum >> 8;

    FlashSectorErase(FLASH_REGISTER_ADDR);
    FlashWrite(FLASH_REGISTER_ADDR, (unsigned char *)pstRegister, sizeof(REGISTER));
}

void GetParam(REGISTER *pstRegister)
{
    LoadParam(pstRegister);
    memcpy(&gstReg, pstRegister, sizeof(REGISTER));
}

void SetParam(REGISTER *pstRegister)
{
    memcpy(&gstReg, pstRegister, sizeof(REGISTER));
    StoreParam(&gstReg);
}

void GetSensorAll(void)
{
    unsigned char   SensVal[SENS_NUM_CHANNEL];

    ReadSensor(SENS_MASK_ALL, SensVal);

    printf("\033[7H\033[J");                                // 화면 클리어 VT100
    printf("[SHELL] Sensors ADC value\r\n");
    printf("Entrance [1][2][3][4]       : [%03d][%03d][%03d][%03d]\r\n", SensVal[SENS_ENTRANCE_1],
                                                                         SensVal[SENS_ENTRANCE_2],
                                                                         SensVal[SENS_ENTRANCE_3],
                                                                         SensVal[SENS_ENTRANCE_4]);
    printf("Exit                        : [%03d]\r\n", SensVal[SENS_EXIT]);
    printf("Up cover                    : [%03d]\r\n", SensVal[SENS_COVER]);
    printf("Brander check               : [%03d]\r\n", SensVal[SENS_TPH]);
}