#include <string.h>
#include <stdbool.h>

#include "hw_types.h"
#include "usblib.h"
#include "usb_bulk_structs.h"
#include "cppi41dma.h"
#include "delay.h"
#include "flash.h"

#include "devcfg.h"
#include "sensor.h"
#include "actuator.h"
#include "cis.h"
#include "tph.h"
#include "decoder.h"
#include "shell.h"
#include "controller.h"

#include "debug.h"


#define REJECT_FEED_MM                  (120)

/* calibration step definition */
#define CALIB_STEP_NONE                 (0)
#define CALIB_STEP_OUT                  (1)
#define CALIB_STEP_IN                   (2)
#define CALIB_STEP_DOUBLE               (3)
#define CALIB_STEP_ABORT                (4)


/* calibration config */
#define CALIB_SENSITIVITY_FACTOR        (10)
#define CALIB_LITERATION                (100)

#define CALIB_NUM_ENTRANCE_SENSOR       (4)

#define TWO_PAPER_FALSE                 (0)
#define TWO_PAPER_TRUE                  (1)


/* ---------- global variable ---------- */
#pragma data_alignment = 8
static unsigned char    gPktBuf[MAX_IMG_LINE * BYTE_PER_SCAN_TRANSACTION];
STATUS			        gstStatus;
unsigned int            gResult;

bool                    gbTwoPaper;

struct
{
    unsigned char       entranceOFF;
    unsigned char       entranceON;
    unsigned char       exitOFF;
    unsigned char       exitON;
    unsigned char       dble;
    unsigned int        entranceVal[4];
    unsigned int        exitVal;
    unsigned int        cnt;
    int                 step;
}   gstCalib;



/* ---------- local fuction prototype ---------- */
static void RunRequest(void);
static void RunScanner(void);
static void RunCalibration(void);
static void RunOMRDecoder(void);
static void RunFactoryInit(void);

static int InjectMedia(void);
static int EjectMedia(void);

static int DrawImage(unsigned int PrintOffset);

static void GetSerialNumber(unsigned char *pSN);
static void SetSerialNumber(unsigned char *pSN);

static void SendResponse(unsigned short    CMDIdx,
                         unsigned int      Result,
                         unsigned char     *pData,
                         unsigned int      Length);

/* state machine entry */
static void _StateIdleHandle(unsigned short CMDIdx);
static void _StateIdentHandle(unsigned short CMDIdx);
static void _StateStandbyHandle(unsigned short CMDIdx);
static void _StateScanHandle(unsigned short CMDIdx);
static void _StateLoadHandle(unsigned short CMDIdx);
static void _StateBrandHandle(unsigned short CMDIdx);
static void _StateCalibHandle(unsigned short CMDIdx);


/* ---------- function definition ---------- */
void CTRLInit(void)
{
    REGISTER    stReg;

    memset((void *)&gstCalib, 0, sizeof(gstCalib));

    /* check integrety for register */
    FlashOpen();
    GetParam(&stReg);
    CheckParam(&stReg);

	ActuatorInit();
	CISInit();
	gstStatus.BrandImageSize = TPHInit();
}

void CTRLRun(void)
{
	/* main loop */
	while (1)
    {
		RunRequest();
		RunScanner();
        RunCalibration();
        RunOMRDecoder();
#if (ENABLE_DEBUG == true)
        RunShell();
#endif
	}
}

static void RunRequest(void)
{
	unsigned short	CMDIdx;

	if (GetUSBCMDIdx(&CMDIdx) == 0)
    {
		if (CMDIdx == CHECK_STATUS)
        {
            gstStatus.Sensor = MaskSensorON(SENS_MASK_ALL);

            SendResponse(CMDIdx, gResult, gstStatus.Status, RSP_PKT_STATUS_LENGTH);

            /* clear each state machine after response */
            if (gstStatus.ScanState == OP_STATE_DONE)
            {
                gstStatus.ScanState = OP_STATE_IDLE;
            }

            if (gstStatus.BrandState == OP_STATE_DONE)
            {
                gstStatus.BrandState = OP_STATE_IDLE;
            }

            if (gstStatus.CalibState == OP_STATE_DONE)
            {
                gstStatus.CalibState = OP_STATE_IDLE;
                gstStatus.DevState   = DEVICE_STATE_STANDBY;

                gstCalib.step = CALIB_STEP_NONE;
            }
		}
        else if (CMDIdx == RESET)
        {
            DBG_PRINT("RESET");

            CTRLInit();

            gstStatus.DevState  = DEVICE_STATE_IDLE;
            gstStatus.ScanState = OP_STATE_IDLE;

            SendResponse(CMDIdx, gResult, NULL, 0);
		}
        else
        {
			switch (gstStatus.DevState)
            {
			case DEVICE_STATE_IDLE:
                _StateIdleHandle(CMDIdx);
				break;

			case DEVICE_STATE_IDENT:
                _StateIdentHandle(CMDIdx);
				break;

			case DEVICE_STATE_STANDBY:
                _StateStandbyHandle(CMDIdx);
                break;

			case DEVICE_STATE_SCAN:
                _StateScanHandle(CMDIdx);
				break;

			case DEVICE_STATE_LOAD:
                _StateLoadHandle(CMDIdx);
				break;

			case DEVICE_STATE_BRAND:
                _StateBrandHandle(CMDIdx);
				break;

            case DEVICE_STATE_CALIBRATION:
                _StateCalibHandle(CMDIdx);
                break;

            default:
                break;
			}
		}

		/* Result clear */
		gResult &= RESULT_RESERVED_0 | RESULT_RESERVED_1 | RESULT_RESERVED_2;
	}
}

static void _StateIdleHandle(unsigned short CMDIdx)
{
    unsigned char   aFWVer[4];
    int             strLen;

    if (CMDIdx == GET_FW_VER)
    {
        DBG_PRINT("GET_FW_VER");

        strLen = strlen(FW_VER);
        memcpy(aFWVer, FW_VER, strLen);

        gstStatus.DevState = DEVICE_STATE_IDENT;
    }
    else
    {
        DBG_PRINT("INVALID 0x%04X", CMDIdx);
        gResult |= RESULT_INVAL_CMD;
    }

    SendResponse(CMDIdx, gResult, aFWVer, RSP_PKT_FW_VER_LENGTH);
}

static void _StateIdentHandle(unsigned short CMDIdx)
{
    unsigned char aSN[RSP_PKT_SN_LENGTH];

    if (CMDIdx == GET_SN)
    {
        DBG_PRINT("GET_SN");

        GetSerialNumber(aSN);

        gstStatus.DevState = DEVICE_STATE_STANDBY;
    }
    else
    {
        DBG_PRINT("INVALID 0x%04X", CMDIdx);
        gResult |= RESULT_INVAL_CMD;
    }

    SendResponse(CMDIdx, gResult, aSN, RSP_PKT_SN_LENGTH);
}

static void _StateStandbyHandle(unsigned short CMDIdx)
{
    REGISTER        stReg;
    unsigned char	*pArg;
    unsigned int	DataLength;
    unsigned char	*pData;
    int             Ret;

    switch (CMDIdx)
    {
    case SCAN_START:
        DBG_PRINT("SCAN_START");

        pArg = GetUSBArg();
        if (pArg[0] == 1)
        {
            DBG_PRINT("receive OMR scan request");
            DecoderInit(&gPktBuf[RSP_PKT_HEADER_LENGTH + OMR_DATA_LENGTH], CIS_NUM_DOT_PER_SCAN, NUM_CIS_CHANNEL);
            gstStatus.DecodeState = OMR_STATE_TOP_MARGIN;
        }

        DataLength = 0;
        pData = NULL;

        gstStatus.DevState = DEVICE_STATE_SCAN;
        gstStatus.ScanState = OP_STATE_WAIT;
        break;

    case MOTOR_ON:
        DBG_PRINT("MOTOR_ON");

        pArg = GetUSBArg();
        MotorONTest(pArg[0], pArg[1]);

        DataLength = 0;
        pData = NULL;
        break;

    case MOTOR_OFF:
        DBG_PRINT("MOTOR_OFF");

        pArg = GetUSBArg();
        MotorOFFTest(pArg[0]);

        DataLength = 0;
        pData = NULL;
        break;

    case LED_CTRL:
        DBG_PRINT("LED_CTRL");

        pArg = GetUSBArg();
        LEDTest(pArg[0]);

        DataLength = 0;
        pData = NULL;
        break;

    case GET_PARAM:
        DBG_PRINT("GET_PARAM");

        GetParam(&stReg);
        DataLength = sizeof(REGISTER);
        pData = (unsigned char *)&stReg;
        break;

    case SET_PARAM:
        DBG_PRINT("SET_PARAM");

        pArg = GetUSBArg();
        SetParam((REGISTER *)pArg);

        DataLength = 0;
        pData = NULL;
        break;

    case SET_SN:
        DBG_PRINT("SET_SN");

        pArg = GetUSBArg();
        SetSerialNumber(pArg);

        DataLength = 0;
        pData = NULL;

        gstStatus.DevState = DEVICE_STATE_IDLE;
        break;

    case CALIBLATION:
        DBG_PRINT("CALIBRATION");

        pArg = GetUSBArg();
        gstCalib.step = *pArg;

        DataLength = 0;
        pData = NULL;

        gstStatus.DevState = DEVICE_STATE_CALIBRATION;
        gstStatus.CalibState = OP_STATE_WAIT;
        break;

    case FACTORY_INIT:
        DBG_PRINT("FACTORY_INIT");

        RunFactoryInit();

        DataLength = 0;
        pData = NULL;

        gstStatus.DevState = DEVICE_STATE_IDLE;
        break;

    case BRAND_INFO:
        DBG_PRINT("BRAND_INFO");

        pArg = GetUSBArg();

        gstStatus.aBrandImageSize[1] = pArg[0];
        gstStatus.aBrandImageSize[0] = pArg[1];
        DBG_PRINT("image size : %d", gstStatus.BrandImageSize);

        TPHGetImageInfo(gstStatus.BrandImageSize);  // image size

        DataLength  = 0;
        pData       = NULL;
        break;

    case BRAND_DATA:
        DBG_PRINT("BRAND_DATA");

        pArg = GetUSBArg();
        Ret  = TPHGetImageData(pArg);
        if (Ret == TPH_RET_SUCCESS)
        {
            DBG_PRINT("Image gettering complete");
        }
        else if (Ret == TPH_RET_FAILURE)
        {
            gResult |= RESULT_SEQUENCE_VIOLATION;
        }

        DataLength  = 0;
        pData       = NULL;
        break;

    default:
        DBG_PRINT("INVALID 0x%04X", CMDIdx);

        gResult |= RESULT_INVAL_CMD;

        DataLength  = 0;
        pData       = NULL;
        break;
    }

    SendResponse(CMDIdx, gResult, pData, DataLength);
}

static void _StateScanHandle(unsigned short CMDIdx)
{
    unsigned char	*pData;

    if (CMDIdx == STOP)
    {
        DBG_PRINT("STOP");

        SendResponse(CMDIdx, gResult, NULL, 0);

        /* 스캐닝 하는 중이었다면 즉시 중단시켜야한다 */
        if (gstStatus.ScanState == OP_STATE_RUN)
        {
            CISDOutDisable();
        }

        if (gstStatus.DecodeState != OMR_STATE_IDLE)
        {
            DBG_PRINT("DECODE abort");
            DecoderInit(&gPktBuf[6], 0, 0);
            gstStatus.DecodeState = OMR_STATE_IDLE;
        }

        gstStatus.ScanState = OP_STATE_IDLE;
        gstStatus.DevState = DEVICE_STATE_STANDBY;
    }
    else if (CMDIdx == GET_IMG)
    {
        DBG_PRINT("GET_IMG");
        SendResponse(CMDIdx, gResult, NULL, gstStatus.NumScanLine * BYTE_PER_SCAN_TRANSACTION);

        gstStatus.DevState = DEVICE_STATE_LOAD;
    }
    else if (CMDIdx == GET_AXIS)
    {
        DBG_PRINT("GET_AXIS");

        pData = GetOMRData();
        SendResponse(CMDIdx, gResult, pData, OMR_DATA_LENGTH);
    }
    else
    {
        DBG_PRINT("INVALID 0x%04X", CMDIdx);

        gResult |= RESULT_INVAL_CMD;

        SendResponse(CMDIdx, gResult, NULL, 0);
    }
}

static void _StateLoadHandle(unsigned short CMDIdx)
{
    int             Ret;
    unsigned short  PrintOffset;
    unsigned char   *pArg;

    if (CMDIdx == INJECT)
    {
        DBG_PRINT("INJECT");

        Ret = InjectMedia();
        if (Ret != CTRL_RET_SUCCESS)
        {
            gResult |= RESULT_TIMEOUT;
            DBG_PRINT("Motor timeout occured in INJECT");
        }

        gstStatus.DevState = DEVICE_STATE_STANDBY;
    }
    else if (CMDIdx == REJECT)
    {
        DBG_PRINT("REJECT");

        Ret = EjectMedia();
        if (Ret != CTRL_RET_SUCCESS)
        {
            gResult |= RESULT_TIMEOUT;
            DBG_PRINT("Motor timeout occured in REJECT");
        }

        gstStatus.DevState = DEVICE_STATE_STANDBY;
    }
    else if (CMDIdx == BRANDING)
    {
        DBG_PRINT("BRANDING");

        pArg = GetUSBArg();
        PrintOffset = (pArg[1] << 8 | pArg[2]);
        DBG_PRINT("printf offset %d", PrintOffset);

        Ret = DrawImage(PrintOffset);
        if (Ret != CTRL_RET_SUCCESS)
        {
            gResult |= RESULT_TIMEOUT;
            DBG_PRINT("Motor timeout occured in BRANDING");
        }
    }
    else
    {
        DBG_PRINT("INVALID 0x%04X", CMDIdx);

        gResult |= RESULT_INVAL_CMD;
    }

    SendResponse(CMDIdx, gResult, NULL, 0);
}

static void _StateBrandHandle(unsigned short CMDIdx)
{
    int             Ret;

    if (CMDIdx == INJECT)
    {
        DBG_PRINT("INJECT");

        Ret = InjectMedia();
        if (Ret != 0)
        {
            gResult |= RESULT_TIMEOUT;
            DBG_PRINT("Motor timeout occured in INJECT");
        }

        gstStatus.DevState = DEVICE_STATE_STANDBY;
    }
    else if (CMDIdx == REJECT)
    {
        DBG_PRINT("REJECT");

        Ret = EjectMedia();
        if (Ret != 0)
        {
            gResult |= RESULT_TIMEOUT;
            DBG_PRINT("Motor timeout occured in REJECT");
        }

        gstStatus.DevState = DEVICE_STATE_STANDBY;
    }
    else
    {
        DBG_PRINT("INVALID 0x%04X", CMDIdx);

        gResult |= RESULT_INVAL_CMD;
    }

    SendResponse(CMDIdx, gResult, NULL, 0);
}

static void _StateCalibHandle(unsigned short CMDIdx)
{
    unsigned char	*pArg;

    if (CMDIdx == CALIBLATION)
    {
        pArg = GetUSBArg();

        if (*pArg == CALIB_STEP_ABORT)
        {
            DBG_PRINT("CALIBRATION ABORT");

            MotorEnd();

            gstStatus.DevState = DEVICE_STATE_STANDBY;
            gstStatus.CalibState = OP_STATE_IDLE;
        }
        else
        {
            DBG_PRINT("CALIBRATION BUSY");

            gResult |= RESULT_BUSY;
        }
    }
    else
    {
        DBG_PRINT("INVALID 0x%04X", CMDIdx);

        gResult |= RESULT_INVAL_CMD;
    }

    SendResponse(CMDIdx, gResult, NULL, 0);
}

static void SendResponse(unsigned short CMDIdx, unsigned int Result, unsigned char *pData, unsigned int Length)
{
    unsigned int PacketLength;

    /* CMD index */
    gPktBuf[0] = (CMDIdx >> 8) & 0xFF;
    gPktBuf[1] = CMDIdx & 0xFF;

    /* Result */
    gPktBuf[2] = (unsigned char)(Result >> 24) & 0xFF;
    gPktBuf[3] = (unsigned char)(Result >> 16) & 0xFF;
    gPktBuf[4] = (unsigned char)(Result >> 8) & 0xFF;
    gPktBuf[5] = (unsigned char)(Result & 0xFF);

    if ((CMDIdx == CHECK_STATUS) ||
        (CMDIdx == GET_FW_VER) ||
        (CMDIdx == GET_PARAM) ||
        (CMDIdx == GET_SN) ||
        (CMDIdx == GET_AXIS))
    {
        memcpy(&gPktBuf[RSP_PKT_HEADER_LENGTH], pData, Length);

        PacketLength = RSP_PKT_HEADER_LENGTH + Length;
    }
    else if (CMDIdx == GET_IMG)
    {
        /*
         * 이미지 버퍼는 패킷버퍼를 직접 사용하기 때문에 메모리 카피를 하지 않는다
         * 성능상의 이유로 get image만 특별히 처리함
         */
        PacketLength = RSP_PKT_HEADER_LENGTH + OMR_DATA_LENGTH + Length;
    }
    else // for the packet no containing data
    {
        PacketLength = RSP_PKT_HEADER_LENGTH + Length;
    }

    SendUSBPacket(gPktBuf, PacketLength);
}

static void RunScanner(void)
{
	unsigned char   *pScanState;
	unsigned int    Ret;

	pScanState  = &(gstStatus.ScanState);

	if (*pScanState == OP_STATE_WAIT)
    {
		/* paper in 센서에 용지가 검지 되었는지 체크 */
		Ret = CheckPaperLocation(SENSE_IN_PAPER_ON);
		if (Ret == SENS_RET_SUCCESS)
        {
			/* 용지가 검지되었으니 스캐너 ON */
            CISDOutEnable(&gPktBuf[RSP_PKT_HEADER_LENGTH + OMR_DATA_LENGTH]);

			*pScanState = OP_STATE_RUN;
		}
	}
    else if (*pScanState == OP_STATE_RUN)
    {
        gstStatus.NumScanLine = CISCheckLine();

		/* 용지가 입수 센서에서 벗어 났는지 체크 */
		Ret = CheckPaperLocation(SENSE_IN_PAPER_OFF);
		if (Ret == SENS_RET_SUCCESS)
        {
            if (gbTwoPaper == false)
            {
                CISDOutDisable();

                if ((gstStatus.DecodeState != OMR_STATE_IDLE) &&
                    (gstStatus.DecodeState != OMR_STATE_DONE))
                {
                    DBG_PRINT("Waiting for OMR complete");
                    *pScanState = OP_STATE_BUSY;
                }
                else
                {
                    *pScanState = OP_STATE_DONE;
                }

                DBG_PRINT("through out for input sensor[%d]", gstStatus.NumScanLine);
			}
            else
            {
				/* 뒤늦게 알아차린 용지 2장 입수를 반환 완료 */
				MotorEnd(); // CCW에 대한 모터 정지

                *pScanState         = OP_STATE_WAIT;
                gbTwoPaper          = false;

                DBG_PRINT("two paper reject");
			}
		}
        else if (Ret == SENS_RET_DETECT)
        {
			if (gstStatus.NumScanLine > MAX_IMG_LINE)
            {
                CISDOutDisable();
				*pScanState = OP_STATE_DONE;

                DBG_PRINT("abnormal termination[%d]", gstStatus.NumScanLine);
			}
		}
        else
        {
			/* 뒤늦게 용지가 2장임을 알아차렸을 때 */
            if (gbTwoPaper == false)
            {
				CISDOutDisable();
                delay(10);

				MotorStart(MOTOR_SPD_MODE_NORMAL, MOTOR_DIRECTION_BACKWARD);
                gbTwoPaper = true;
			}
            else
            {
				/* do nothing */
			}
		}
	}
    else if (*pScanState == OP_STATE_BUSY)
    {
        if (gstStatus.DecodeState == OMR_STATE_DONE)
        {
            DBG_PRINT("decode & scan complete");
            *pScanState = OP_STATE_DONE;
        }
    }
}

void RunCalibration(void)
{
    REGISTER        reg;
    unsigned char   sensVal[SENS_NUM_CHANNEL];
    unsigned char   *pCalibState = &(gstStatus.CalibState);
    unsigned char   entrance;
    int             sensorList[4] = { SENS_ENTRANCE_1, SENS_ENTRANCE_2, SENS_ENTRANCE_3, SENS_ENTRANCE_4 };
    int             cnt;
    unsigned int   entVal[4];
    unsigned int   exitVal;

    if (gstCalib.step == CALIB_STEP_NONE)
    {
        return;
    }
    else
    {
        if (*pCalibState == OP_STATE_WAIT)
        {
            if (gstCalib.step == CALIB_STEP_OUT)
            {
                gstCalib.cnt     = 0;
                gstCalib.exitVal = 0;
                memset(gstCalib.entranceVal, 0, sizeof(gstCalib.entranceVal));
                
                *pCalibState = OP_STATE_RUN;
            }
            else if (gstCalib.step == CALIB_STEP_IN)
            {
                GetParam(&reg);

                ReadSensor(SENS_MASK_PAPER_IN, sensVal);

                if ((sensVal[SENS_ENTRANCE_1] > reg.entranceOFF) ||
                    (sensVal[SENS_ENTRANCE_2] > reg.entranceOFF) ||
                    (sensVal[SENS_ENTRANCE_3] > reg.entranceOFF) ||
                    (sensVal[SENS_ENTRANCE_4] > reg.entranceOFF))
                {
                    MotorStart(MOTOR_SPD_MODE_NORMAL, MOTOR_DIRECTION_FORWARD);

                    gstCalib.cnt     = 0;
                    gstCalib.exitVal = 0;
                    memset(gstCalib.entranceVal, 0, sizeof(gstCalib.entranceVal));
                    *pCalibState = OP_STATE_RUN;
                }
            }
            else if (gstCalib.step == CALIB_STEP_DOUBLE)
            {
                gstCalib.cnt     = 0;
                gstCalib.exitVal = 0;
                memset(gstCalib.entranceVal, 0, sizeof(gstCalib.entranceVal));
                *pCalibState = OP_STATE_RUN;
            }
        }
        else if (*pCalibState == OP_STATE_RUN)
        {
            if (gstCalib.step == CALIB_STEP_OUT)
            {
                ReadSensor(SENS_MASK_PAPER_IN | (1 << SENS_EXIT), sensVal);

                /* accumulation */
                for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                {
                    gstCalib.entranceVal[cnt] += sensVal[sensorList[cnt]];
                }
                gstCalib.exitVal += sensVal[SENS_EXIT];
                
                gstCalib.cnt++;
                if (gstCalib.cnt == CALIB_LITERATION)
                {
                    /* get average */
                    for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                    {
                        entVal[cnt] = gstCalib.entranceVal[cnt] / CALIB_LITERATION;
                    }
                    exitVal = gstCalib.exitVal / CALIB_LITERATION;

                    /* get max value */
                    entrance = 0;
                    for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                    {
                        if (entrance < entVal[cnt])
                        {
                            entrance = entVal[cnt];
                        }
                    }

                    /* compare previous sense */
                    if ((gstCalib.entranceOFF == 0) ||
                        (gstCalib.entranceOFF > entrance))
                    {
                        gstCalib.entranceOFF = entrance;
                    }

                    if ((gstCalib.exitOFF == 0) ||
                        (gstCalib.exitOFF < exitVal))
                    {
                        gstCalib.exitOFF = exitVal;
                    }

                    /* parameter update */
                    GetParam(&reg);
                    reg.entranceOFF = gstCalib.entranceOFF + CALIB_SENSITIVITY_FACTOR;
                    reg.exitOFF     = gstCalib.exitOFF     - CALIB_SENSITIVITY_FACTOR;
                    SetParam(&reg);

                    DBG_PRINT("entrance OFF : %d\r\n", reg.entranceOFF);
                    DBG_PRINT("exit     OFF : %d\r\n", reg.exitOFF);

                    *pCalibState = OP_STATE_DONE;
                }
            }
            else if (gstCalib.step == CALIB_STEP_IN)
            {
                ReadSensor(SENS_MASK_PAPER_IN | (1 << SENS_EXIT), sensVal);

                if (sensVal[SENS_EXIT] > (gstCalib.exitOFF - CALIB_SENSITIVITY_FACTOR))
                {
                    return;
                }
                else
                {
                    if (gstCalib.cnt == 0)
                    {
                        MotorEnd();
                    }
                }

                /*
                 * NOTE. it should has the value of entrance OFF
                 */

                /* accumulation */
                for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                {
                    gstCalib.entranceVal[cnt] += sensVal[sensorList[cnt]];
                }
                gstCalib.exitVal += sensVal[SENS_EXIT];

                gstCalib.cnt++;
                if (gstCalib.cnt == CALIB_LITERATION)
                {
                    /* get average */
                    for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                    {
                        entVal[cnt] = gstCalib.entranceVal[cnt] / CALIB_LITERATION;
                    }
                    exitVal = gstCalib.exitVal / CALIB_LITERATION;

                    /* get min value */
                    entrance = 0xFF;
                    for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                    {
                        if ((entrance > entVal[cnt]) && (gstCalib.entranceOFF < entVal[cnt]))
                        {
                            entrance = entVal[cnt];
                        }
                    }

                    /* compare privous sense */
                    if ((gstCalib.entranceON == 0) || (gstCalib.entranceON > entrance))
                    {
                        gstCalib.entranceON = entrance;
                    }

                    if ((gstCalib.exitON == 0) ||
                        (gstCalib.exitOFF > exitVal) && (gstCalib.exitON < exitVal))
                    {
                        gstCalib.exitON = exitVal;
                    }

                    GetParam(&reg);
                    reg.entranceON = gstCalib.entranceON - CALIB_SENSITIVITY_FACTOR;
                    reg.exitON     = gstCalib.exitON     + CALIB_SENSITIVITY_FACTOR;
                    SetParam(&reg);

                    DBG_PRINT("entrance ON : %d\r\n", reg.entranceON);
                    DBG_PRINT("exit     ON : %d\r\n", reg.exitON);

                    EjectMedia();

                    *pCalibState = OP_STATE_DONE;
                }
            }
            else if (gstCalib.step == CALIB_STEP_DOUBLE)
            {
                ReadSensor(SENS_MASK_PAPER_IN, sensVal);

                /*
                 * NOTE. it should has the value of entrance ON
                 */

                if ((sensVal[SENS_ENTRANCE_1] > gstCalib.entranceON) ||
                    (sensVal[SENS_ENTRANCE_2] > gstCalib.entranceON) ||
                    (sensVal[SENS_ENTRANCE_3] > gstCalib.entranceON) ||
                    (sensVal[SENS_ENTRANCE_4] > gstCalib.entranceON))
                {
                    /* accumulation */
                    for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                    {
                        gstCalib.entranceVal[cnt] += sensVal[sensorList[cnt]];
                    }
                    gstCalib.exitVal += sensVal[SENS_EXIT];

                    gstCalib.cnt++;
                    if (gstCalib.cnt == CALIB_LITERATION)
                    {
                        /* get average */
                        for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                        {
                            entVal[cnt] = gstCalib.entranceVal[cnt] / CALIB_LITERATION;
                        }
                        exitVal = gstCalib.exitVal / CALIB_LITERATION;

                        /* get min value */
                        entrance = 0xFF;
                        for (cnt = 0; cnt < CALIB_NUM_ENTRANCE_SENSOR; cnt++)
                        {
                            DBG_PRINT("!!! entrance : %d, %d\r\n", entVal[cnt], cnt);
                            if ((entrance > entVal[cnt]) && (gstCalib.entranceON < entVal[cnt]))
                            {
                                entrance = entVal[cnt];                                
                            }
                        }

                        /* compare with privous */
                        if ((gstCalib.dble == 0) || (gstCalib.dble > entrance))
                        {
                            gstCalib.dble = entrance;
                        }

                        GetParam(&reg);
                        reg.doubleMedia = gstCalib.dble - CALIB_SENSITIVITY_FACTOR;
                        SetParam(&reg);

                        DBG_PRINT("double media %d\r\n", reg.doubleMedia);

                        *pCalibState = OP_STATE_DONE;
                    }
                }
            }
        }
    }
}

static void RunOMRDecoder(void)
{
    unsigned char   *pDecodeState;
    unsigned char   *pScanState;
    unsigned short  ScanLine;
    int             Ret;

    pScanState = &(gstStatus.ScanState);
    if ((*pScanState != OP_STATE_IDLE) &&
        (*pScanState != OP_STATE_WAIT))
    {
        ScanLine     = CISCheckLine();
        pDecodeState = &(gstStatus.DecodeState);
        switch (*pDecodeState)
        {
        case OMR_STATE_TOP_MARGIN:
            Ret = DecoderFindTopEdge(ScanLine);
            if (Ret == DECODER_RET_SUCCESS)
            {
                DBG_PRINT("found top margin");
                *pDecodeState = OMR_STATE_SIDE_MARGIN;
            }
            break;

        case OMR_STATE_SIDE_MARGIN:
            Ret = DecoderFindLeftRightEdge(ScanLine);
            if (Ret == DECODER_RET_SUCCESS)
            {
                DBG_PRINT("found LR margin");
                *pDecodeState = OMR_STATE_DIRECTION;
            }
            break;

        case OMR_STATE_DIRECTION:
            Ret = DecoderFindDirection(ScanLine);
            if (Ret == DECODER_RET_SUCCESS)
            {
                DBG_PRINT("found direction");
                *pDecodeState = OMR_STATE_REF_MARK;
            }
            else if (Ret == DECODER_RET_FAILURE)
            {
                DBG_PRINT("invalid media");
                *pDecodeState = OMR_STATE_DONE;
            }
            break;

        case OMR_STATE_REF_MARK:
            Ret = DecoderFindRefMark(ScanLine);
            if (Ret == DECODER_RET_SUCCESS)
            {
                *pDecodeState = OMR_STATE_USR_MARK;
            }
            break;

        case OMR_STATE_USR_MARK:
            Ret = DecoderFindUserMark(ScanLine);
            if (Ret == DECODER_RET_OUTSTANDING)
            {
                *pDecodeState = OMR_STATE_REF_MARK;
            }
            else if (Ret == DECODER_RET_SUCCESS)
            {
                DBG_PRINT("done");
                *pDecodeState = OMR_STATE_DONE;
            }
            break;

        default:
            break;
        }

        if ((*pScanState == OP_STATE_BUSY) &&
            (Ret == DECODER_RET_WAIT))
        {
            DBG_PRINT("decoding fail");
            *pDecodeState = OMR_STATE_DONE;
        }
    }
}

static int InjectMedia(void)
{
    unsigned int	MotorTick;
    unsigned int    Tick;
	int				Ret             = CTRL_RET_SUCCESS;

	/* run main motor with forward direction for store media */
	MotorStart(MOTOR_SPD_MODE_NORMAL, MOTOR_DIRECTION_FORWARD);

    /* wait for detect end side of media */
    Tick = GetTimerTick();
    while (MaskSensorOFF(1 << SENS_EXIT) == 0)
    {
        if ((GetTimerTick() - Tick) > MOTOR_TIMEOUT_MS)
        {
            Ret = CTRL_RET_FORWARD_TIMEOUT;
            goto TIMEOUT;
        }
    }

	/* move media by path length additionally */
    MotorTick = GetMotorTick();
    while ((GetMotorTick() - MotorTick) < MM_TO_STEP(DISTANCE_ROLLER_TO_ROLLER));

TIMEOUT:
    /* main motor stop */
	MotorEnd();

    DBG_PRINT(" ");

    return Ret;
}

static int EjectMedia(void)
{
	int				Ret             = CTRL_RET_SUCCESS;
	unsigned int	TimerTick;
    unsigned int    MotorTick;
    unsigned short  TgtSensor       = (1 << SENS_ENTRANCE_1) |
                                      (1 << SENS_ENTRANCE_2) |
                                      (1 << SENS_ENTRANCE_3) |
                                      (1 << SENS_ENTRANCE_4) |
                                      (1 << SENS_EXIT);
    unsigned short  RetSensor;
    int             MarkCnt         = 0;

    /* run main motor with backward direction for eject media to user */
	MotorStart(MOTOR_SPD_MODE_NORMAL, MOTOR_DIRECTION_BACKWARD);

    /* wait for media detected by input sensors and tph sensor */
    TimerTick = GetTimerTick();
    while (true)
    {
        RetSensor = MaskSensorON(TgtSensor);
        if ((RetSensor == ((1 << SENS_ENTRANCE_1) | (1 << SENS_ENTRANCE_2) | (1 << SENS_EXIT))) ||
            (RetSensor == ((1 << SENS_ENTRANCE_2) | (1 << SENS_ENTRANCE_3) | (1 << SENS_EXIT))) ||
            (RetSensor == ((1 << SENS_ENTRANCE_3) | (1 << SENS_ENTRANCE_4) | (1 << SENS_EXIT))))
        {
            break;
        }

        if ((GetTimerTick() - TimerTick) > MOTOR_TIMEOUT_MS)
        {
            Ret = CTRL_RET_BACKWARD_TIMEOUT;
            goto TIMEOUT;
        }
    }

    TimerTick = GetTimerTick();
    while (true)
    {
        while (MaskSensorON((1 << SENS_EXIT)) > 0)
        {
            if ((GetTimerTick() - TimerTick) > MOTOR_TIMEOUT_MS)
            {
                Ret = CTRL_RET_BACKWARD_TIMEOUT;
                goto TIMEOUT;
            }
        }

        TimerTick = GetTimerTick();
        MotorTick = GetMotorTick();
        while ((GetMotorTick() - MotorTick) < MM_TO_STEP(DISTANCE_ENTRANCE_EXIT_SENS))
        {
            if ((GetTimerTick() - TimerTick) > MOTOR_TIMEOUT_MS)
            {
                Ret = CTRL_RET_BACKWARD_TIMEOUT;
                goto TIMEOUT;
            }
        }

        if (MaskSensorOFF((1 << SENS_EXIT)) > 0)
        {
            break;
        }
        else
        {
            MarkCnt++;
        }
    }

    if (MarkCnt > 1)
    {
        Ret = CTRL_RET_BACKWARD_TIMEOUT;
    }

TIMEOUT:
	/* main motor stop */
	MotorEnd();

    DBG_PRINT(" ");

	return Ret;
}

static int DrawImage(unsigned int PrintOffset)
{
    unsigned int    MotorTick;
    unsigned int    TimerTick;
    unsigned short  sens = (1 << SENS_EXIT) |
                           (1 << SENS_ENTRANCE_1) |
                           (1 << SENS_ENTRANCE_2) |
                           (1 << SENS_ENTRANCE_3) |
                           (1 << SENS_ENTRANCE_4);
    int             ret  = CTRL_RET_SUCCESS;

	/* move the TPH to print position */
	BranderTphPosition();

    /* run main motor with brand mode */
    MotorStart(MOTOR_SPD_MODE_BRAND, MOTOR_DIRECTION_BACKWARD);

    /* waiting for media move by vertical offset. */
    MotorTick = GetMotorTick();
    TimerTick = GetTimerTick();
    while ((GetMotorTick() - MotorTick) < MM_TO_STEP(LOCATE_PRINT_POS_Y))
    {
        if ((GetTimerTick() - TimerTick) > MOTOR_TIMEOUT_MS)
        {
            MotorEnd();
            return CTRL_RET_BACKWARD_TIMEOUT;
        }
    }

    /* image out */
    TPHBrand();

    /* main motor stop */
    MotorEnd();

    /* comeback the TPH */
    BranderHomePosition();

    /* wait for take the media off */
    if (MaskSensorOFF(sens) != sens)
    {
        MotorStart(MOTOR_SPD_MODE_BRAND, MOTOR_DIRECTION_FORWARD);

        TimerTick = GetTimerTick();
        while (MaskSensorOFF(sens) != sens)
        {
            if ((GetTimerTick() - TimerTick) > MOTOR_TIMEOUT_MS)
            {
                ret = CTRL_RET_FORWARD_TIMEOUT;
                break;
            }
        }

        MotorEnd();
    }

    DBG_PRINT("branding finish");

    return ret;
}

static void GetSerialNumber(unsigned char *pSN)
{
	FlashRead(FLASH_DEVICE_SN_ADDR, pSN, RSP_PKT_SN_LENGTH);
	for (int i = 0; i < RSP_PKT_SN_LENGTH; i++)
    {
		if (pSN[i] > 0x80)
        {
			pSN[i] = 0;
		}
	}

    DBG_PRINT(" ");
}

static void SetSerialNumber(unsigned char *pSN)
{
	FlashSectorErase(FLASH_DEVICE_SN_ADDR);
	FlashWrite(FLASH_DEVICE_SN_ADDR, pSN, RSP_PKT_SN_LENGTH);

    DBG_PRINT(" ");
}

static void RunFactoryInit(void)
{
	/* clear register */
	FlashSectorErase(FLASH_REGISTER_ADDR);

	/* clear serial number */
	FlashSectorErase(FLASH_DEVICE_SN_ADDR);

	/* TODO. set initial serial number */

	/* run device init */
	CTRLInit();

    DBG_PRINT(" ");
}
