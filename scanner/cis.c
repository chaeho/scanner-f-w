#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include "soc_AM1808.h"
#include "evmAM1808.h"
#include "hw_syscfg0_AM1808.h"
#include "interrupt.h"
#include "gpio.h"
#include "timer.h"
#include "flash.h"
#include "spi_io.h"

#include "vpif.h"
#include "usb_bulk_structs.h"

#include "delay.h"

#include "devcfg.h"
#include "sensor.h"
#include "cis.h"
#include "actuator.h"

#include "debug.h"


#define MUX_CPLD_CLK_CFG                    (SYSCFG_PINMUX13_PINMUX13_7_4_OBSCLK0  << \
							                 SYSCFG_PINMUX13_PINMUX13_7_4_SHIFT)
#define RLED_ENABLE_PIN                     (SYSCFG_PINMUX1_PINMUX1_15_12_GPIO0_4  << \
							                 SYSCFG_PINMUX1_PINMUX1_15_12_SHIFT)
#define AFE_DOE_PIN                         (SYSCFG_PINMUX3_PINMUX3_31_28_GPIO8_1  << \
							                 SYSCFG_PINMUX3_PINMUX3_31_28_SHIFT)
#define SI_PLD_PIN		                    (SYSCFG_PINMUX2_PINMUX2_27_24_GPIO1_9  << \
							                 SYSCFG_PINMUX2_PINMUX2_27_24_SHIFT)
#define CPLD_RESET	                        (SYSCFG_PINMUX3_PINMUX3_27_24_GPIO8_2  << \
										     SYSCFG_PINMUX3_PINMUX3_27_24_SHIFT)

#define NUM_VALIDITY_CHECK_POINT            (4)
#define WATERMARK_DOT_VALIDITY              (0)

#define VPIF_INT_DELAY_CPL_CYCLE            (1)

#define AFE_DATA_CONVERT_SPI(ADDR, DATA)    (((ADDR) << 12) | ((DATA & 0x01FF)))


bool	        gbScanON;

unsigned char   *gpImgBuf;
unsigned int	gImgCnt;

unsigned int	gSIFlag;


/* local function prototype */
void CPLDInit(void);
void VPIFInit(void);
void AFEInit(void);
void LEDInit(void);

static void VPIFIsr(void);
static void CPLDTimerIsr(void);


/* local function definition */
void CISInit(void)
{
    CPLDInit();
    VPIFInit();
    AFEInit();
    LEDInit();

    DBG_PRINT(" ");
}

void CPLDInit(void)
{
    unsigned int savePinmux;

    IntRegister(SYS_INT_TINT12_1, CPLDTimerIsr);
    IntChannelSet(SYS_INT_TINT12_1, 10);
    IntSystemEnable(SYS_INT_TINT12_1);

    //PLL Clock out - 50mhz
    HWREG(0x01C1417C) = 0x00;
    HWREG(0x01C11104) = 0x1D;   // OBSCLK(PLL obserbation clock)로 PLL0_SYSCLK7 선택
    HWREG(0x01C1417C) = 0x110;

    savePinmux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(13)) &
                        ~(SYSCFG_PINMUX13_PINMUX13_7_4_OBSCLK0));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(13)) = (MUX_CPLD_CLK_CFG | savePinmux);

    /** SI mux setup **/
    savePinmux = HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(2)) &
                        ~(SYSCFG_PINMUX2_PINMUX2_27_24);
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(2)) = (SI_PLD_PIN | savePinmux);

    /** nScanner mux setup **/
    savePinmux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(3)) &
                    ~(SYSCFG_PINMUX3_PINMUX3_27_24));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(3)) = (CPLD_RESET | savePinmux);

    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_PLD_SI, GPIO_PIN_LOW);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_PLD_SI, GPIO_DIR_OUTPUT);

    //nScanner
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_PLD_RESET, GPIO_PIN_LOW);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_PLD_RESET, GPIO_DIR_OUTPUT);

    /*
     * CPLD reset 이슈로 의미가 없던 scanner 핀을 리셋신호로 사용함
     * high에서 low로 떨어지는 edge를 detect 했을때 CPLD가 리셋되도록 workaround 함
     * low acitive
     */
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_PLD_RESET, GPIO_PIN_HIGH);
    delay(1);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_PLD_RESET, GPIO_PIN_LOW);
    delay(1);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_PLD_RESET, GPIO_PIN_HIGH);

    /** 327us msec Interval Timer for CPLD **/
    TimerConfigure(SOC_TMR_1_REGS, TMR_CFG_32BIT_UNCH_CLK_BOTH_INT);
    TimerPeriodSet(SOC_TMR_1_REGS, TMR_TIMER12, 24 * CPLD_PERIOD);
    TimerIntEnable(SOC_TMR_1_REGS, TMR_INT_TMR12_NON_CAPT_MODE);
    TimerEnable(SOC_TMR_1_REGS, TMR_TIMER12, TMR_ENABLE_CONT);

    DBG_PRINT(" ");
}

void VPIFInit(void)
{
    IntRegister(SYS_INT_VPIF, VPIFIsr);
    IntChannelSet(SYS_INT_VPIF, 4);
    IntSystemEnable(SYS_INT_VPIF);

    VPIFCaptureCapmodeModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_0, VPIF_CAPTURE_RAW);
    VPIFCaptureCapmodeModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_1, VPIF_CAPTURE_RAW);

    VPIFCaptureYcmuxModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_0, VPIF_YC_NONMUXED);
    VPIFCaptureYcmuxModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_1, VPIF_YC_NONMUXED);

    VPIFInterruptEnable(SOC_VPIF_0_REGS, VPIF_FRAMEINT_CH1);
    VPIFInterruptEnableSet(SOC_VPIF_0_REGS, VPIF_FRAMEINT_CH1);

    VPIFCaptureIntframeConfig(SOC_VPIF_0_REGS, VPIF_CHANNEL_0, VPIF_FRAME_INTERRUPT_TOP);
    VPIFCaptureIntframeConfig(SOC_VPIF_0_REGS, VPIF_CHANNEL_1, VPIF_FRAME_INTERRUPT_TOP);

    VPIFCaptureHancDisable(SOC_VPIF_0_REGS, VPIF_CHANNEL_0);
    VPIFCaptureHancDisable(SOC_VPIF_0_REGS, VPIF_CHANNEL_1);

    VPIFCaptureVancDisable(SOC_VPIF_0_REGS, VPIF_CHANNEL_0);
    VPIFCaptureVancDisable(SOC_VPIF_0_REGS, VPIF_CHANNEL_1);

    VPIFCaptureIntrprogModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_0, VPIF_CAPTURE_PROGRESSIVE);
    VPIFCaptureIntrprogModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_1, VPIF_CAPTURE_PROGRESSIVE);

    VPIFCaptureClkedgeModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_0, VPIF_CLKEDGE_FALLING);
    VPIFCaptureClkedgeModeSelect(SOC_VPIF_0_REGS, VPIF_CHANNEL_1, VPIF_CLKEDGE_FALLING);

    VPIFCaptureFieldframeModeSelect(SOC_VPIF_0_REGS, VPIF_FRAME_BASED);
    VPIFCaptureRawHvinvSet(SOC_VPIF_0_REGS, 0);
    VPIFCaptureRawVvinvSet(SOC_VPIF_0_REGS, 0);
    VPIFCaptureRawFidinvSet(SOC_VPIF_0_REGS, 0);
    VPIFCaptureRawIntlineConfig(SOC_VPIF_0_REGS, 1);
    VPIFCaptureRawDatawidthConfig(SOC_VPIF_0_REGS, VPIF_RAW_EIGHT_BPS);

    VPIFPinMuxSetup();

    DBG_PRINT(" ");
}

void AFERegInitSet(unsigned short DACR, unsigned short Gain, unsigned char BotTopSel)
{
    //D6 : Clamp Internal						: 0
    //D5 : 3channel off							: 0
    //D4 : CDS SHA Mode							: 0
    //D3 : reserved								: 0
    //D2 : Power down off						: 0
    //D1 : Full-scale input range 2V			: 1
    //D0 : 1 byte Out Mode						: 1
    SPI0Transfer16b(SPI0_DEVICE_ID_AFE, AFE_DATA_CONVERT_SPI(0x00, 0x01), SPI_IO_FLAG_NO_RESPONSE);
    //D6 : RED Channel On
    //D5 : GREEN Channel Off
    //D0~D3 : Clamp voltage  : 중간값
    SPI0Transfer16b(SPI0_DEVICE_ID_AFE, AFE_DATA_CONVERT_SPI(0x01, (0x03 | BotTopSel)), SPI_IO_FLAG_NO_RESPONSE);
    SPI0Transfer16b(SPI0_DEVICE_ID_AFE, AFE_DATA_CONVERT_SPI(0x02, Gain), SPI_IO_FLAG_NO_RESPONSE);
    SPI0Transfer16b(SPI0_DEVICE_ID_AFE, AFE_DATA_CONVERT_SPI(0x03, Gain), SPI_IO_FLAG_NO_RESPONSE);
    SPI0Transfer16b(SPI0_DEVICE_ID_AFE, AFE_DATA_CONVERT_SPI(0x05, DACR), SPI_IO_FLAG_NO_RESPONSE);
    SPI0Transfer16b(SPI0_DEVICE_ID_AFE, AFE_DATA_CONVERT_SPI(0x06, DACR), SPI_IO_FLAG_NO_RESPONSE);
}

void AFEInit(void)
{
    unsigned int    savePinmux;
    REGISTER        stReg;

    /** AFE DATA OUT EMABLE **/
    savePinmux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(3)) &
                        ~(SYSCFG_PINMUX3_PINMUX3_31_28));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(3)) = (AFE_DOE_PIN | savePinmux);

    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_AFE_DOE, GPIO_PIN_LOW);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_AFE_DOE, GPIO_DIR_OUTPUT);

    GetParam(&stReg);
    AFERegInitSet(stReg.DACR, stReg.gain, 0x60);

    DBG_PRINT("DACR %d : Gain %d", stReg.DACR, stReg.gain);
}

void CISDOutEnable(unsigned char *pImgBuf)
{
    DBG_PRINT("scan start");

    gpImgBuf  = pImgBuf;
    gImgCnt   = 0;
    gbScanON  = true;

	VPIFDMASetup();
	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_CIS_LED, GPIO_PIN_HIGH);
	MotorStart(MOTOR_SPD_MODE_NORMAL, MOTOR_DIRECTION_FORWARD);

    /* VPIF capture enable */
    VPIFCaptureChanenEnable(SOC_VPIF_0_REGS, VPIF_CHANNEL_0);
    VPIFCaptureChanenEnable(SOC_VPIF_0_REGS, VPIF_CHANNEL_1);
}

unsigned int CISDOutDisable(void)
{
	/* 스캐닝 종료 */
    VPIFCaptureChanenDisable(SOC_VPIF_0_REGS, VPIF_CHANNEL_0);

	gbScanON  = false;

	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_CIS_LED, GPIO_PIN_LOW);

	MotorEnd();

    DBG_PRINT("scan end");

    return gImgCnt * BYTE_PER_SCAN_TRANSACTION;
}

unsigned short CISCheckLine(void)
{
    return gImgCnt;
}

//
// Video Port Interface Interrupt processsing
//
static void VPIFIsr(void)
{
    HWREG(SOC_VPIF_0_REGS + C0TLUMA) = (unsigned int)(gpImgBuf + (gImgCnt * BYTE_PER_SCAN_TRANSACTION));
    HWREG(SOC_VPIF_0_REGS + C0IMGOFFSET) = BYTE_PER_SCAN_TRANSACTION;

    gImgCnt++;

	/* clear interrupt */
    IntSystemStatusClear(SYS_INT_VPIF);
    VPIFInterruptStatusClear(SOC_VPIF_0_REGS, VPIF_FRAMEINT_CH1);
}

//
// CPLD Timer Interruptn processing
//
void CPLDTimerIsr(void)
{
    volatile int i = 60;

	if (gbScanON == true)
    {
		if (gSIFlag == 1)
        {
			GPIOPinWrite(SOC_GPIO_0_REGS, PIN_PLD_SI, GPIO_PIN_HIGH);
            while (i--);
            GPIOPinWrite(SOC_GPIO_0_REGS, PIN_PLD_SI, GPIO_PIN_LOW);

			gSIFlag = 0;
		}
        else if (gSIFlag == 0)
        {
			gSIFlag = 1;
		}
	}

    /* Clear the interrupt status in AINTC and in timer */
    IntSystemStatusClear(SYS_INT_TINT12_1);
    TimerIntStatusClear(SOC_TMR_1_REGS, TMR_INT_TMR12_NON_CAPT_MODE);
}

void VPIFDMASetup(void)
{
    VPIFDMARequestSizeConfig(SOC_VPIF_0_REGS, VPIF_REQSIZE_TWO_FIFTY_SIX);
	VPIFEmulationControlSet(SOC_VPIF_0_REGS, VPIF_FREE);
    VPIFCaptureFBConfig(SOC_VPIF_0_REGS,
						VPIF_CHANNEL_0,
						VPIF_TOP_FIELD,
						VPIF_LUMA,
						(unsigned int)gpImgBuf,
						BYTE_PER_SCAN_TRANSACTION);
}

void LEDInit(void)
{
    unsigned int savePinmux;

    /** RLED_ENABLE **/
    savePinmux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(1)) &
                        ~(SYSCFG_PINMUX1_PINMUX1_15_12));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(1)) = (RLED_ENABLE_PIN | savePinmux);

    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_CIS_LED, GPIO_PIN_LOW);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_CIS_LED, GPIO_DIR_OUTPUT);
}

void LEDTest(unsigned int ONOFF)
{
    if (ONOFF == 1)
    {
        GPIOPinWrite(SOC_GPIO_0_REGS, PIN_CIS_LED, GPIO_PIN_HIGH);
    }
    else
    {
        GPIOPinWrite(SOC_GPIO_0_REGS, PIN_CIS_LED, GPIO_PIN_LOW);
    }
}