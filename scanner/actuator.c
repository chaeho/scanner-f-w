#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "delay.h"
#include "soc_AM1808.h"
#include "hw_syscfg0_AM1808.h"
#include "evmAM1808.h"
#include "timer.h"
#include "interrupt.h"
#include "gpio.h"

#include "tph.h"
#include "devcfg.h"
#include "actuator.h"

#include "debug.h"


#define MOTOR_PIN_GROUP1                (SYSCFG_PINMUX11_PINMUX11_7_4_GPIO5_14 << \
							             SYSCFG_PINMUX11_PINMUX11_7_4_SHIFT) | \
						                (SYSCFG_PINMUX11_PINMUX11_3_0_GPIO5_15  << \
							             SYSCFG_PINMUX11_PINMUX11_3_0_SHIFT)
#define MOTOR_PIN_GROUP2                (SYSCFG_PINMUX10_PINMUX10_31_28_GPIO4_0  << \
							             SYSCFG_PINMUX10_PINMUX10_31_28_SHIFT) | \
						                (SYSCFG_PINMUX10_PINMUX10_27_24_GPIO4_1  << \
							             SYSCFG_PINMUX10_PINMUX10_27_24_SHIFT) | \
						                (SYSCFG_PINMUX10_PINMUX10_23_20_GPIO4_2  << \
							             SYSCFG_PINMUX10_PINMUX10_23_20_SHIFT) | \
						                (SYSCFG_PINMUX10_PINMUX10_19_16_GPIO4_3  << \
							             SYSCFG_PINMUX10_PINMUX10_19_16_SHIFT)
#define MOTOR_PIN_GROUP3	            (SYSCFG_PINMUX4_PINMUX4_15_12_GPIO1_4  << \
							             SYSCFG_PINMUX4_PINMUX4_15_12_SHIFT)
#define COVER_OPEN_PIN                  (SYSCFG_PINMUX1_PINMUX1_7_4_GPIO0_6  << \
									     SYSCFG_PINMUX1_PINMUX1_7_4_SHIFT)
#define TXLED_PIN	                    (SYSCFG_PINMUX1_PINMUX1_19_16_GPIO0_3  << \
									     SYSCFG_PINMUX1_PINMUX1_19_16_SHIFT)
#define TEST_ENABLE_PIN                 (SYSCFG_PINMUX17_PINMUX17_7_4_GPIO7_8  << \
							             SYSCFG_PINMUX17_PINMUX17_7_4_SHIFT) | \
							            (SYSCFG_PINMUX17_PINMUX17_3_0_GPIO7_9  << \
							             SYSCFG_PINMUX17_PINMUX17_3_0_SHIFT)
#define SOL_ENABLE_PIN	                (SYSCFG_PINMUX1_PINMUX1_31_28_GPIO0_0  << \
							             SYSCFG_PINMUX1_PINMUX1_31_28_SHIFT) | \
							             (SYSCFG_PINMUX1_PINMUX1_3_0_GPIO0_7  << \
							             SYSCFG_PINMUX1_PINMUX1_3_0_SHIFT)

#define MEASURE_REPEAT_CNT              (5)


static int      gClkPhase;
unsigned int	gMotorTimerCnt;
unsigned char	gMotorSpdMode;
unsigned char	gAccelTableIdx;


#if (MOTOR_SPD_SELECT == MOTOR_SPD_730MM)
#define MOTOR_TIMER_PPS_INIT		(6631)
#define MOTOR_TIMER_PPS_BRAND		(2000)
#define MOTOR_TABLE_LENGTH          (130)

unsigned int MotorAccelTable[MOTOR_TABLE_LENGTH] = {
	MOTOR_TIMER_PPS_INIT,    2747,   2107,   1777,   1565,   1415,   1301,   1211,   1137,   1076,
    1023,    978,    938,    902,    870,    842,    816,    792,    771,    750,
     732,    715,    699,    684,    669,    656,    644,    632,    621,    610,
     600,    590,    581,    572,    564,    556,    548,    541,    534,    527,
     521,    514,    508,    502,    497,    491,    486,    481,    476,    471,
     466,    462,    457,    453,    449,    445,    441,    437,    433,    429,
     426,    422,    419,    416,    412,    409,    406,    403,    400,    397,
     394,    392,    389,    386,    384,    381,    379,    376,    374,    371,
     369,    367,    365,    362,    360,    358,    356,    354,    352,    350,
     348,    346,    344,    342,    341,    339,    337,    335,    334,    332,
     330,    329,    327,    325,    324,    322,    321,    319,    318,    316,
     315,    314,    312,    311,    309,    308,    307,    305,    304,    303,
     302,    300,    299,    298,    297,    295,    294,    293,    292,    291
};
#elif (MOTOR_SPD_SELECT == MOTOR_SPD_650MM)
#define MOTOR_TIMER_PPS_INIT		(7598)
#define MOTOR_TIMER_PPS_BRAND		(1400)
#define MOTOR_TABLE_LENGTH          (130)

const unsigned int MotorAccelTable[MOTOR_TABLE_LENGTH] = {
    MOTOR_TIMER_PPS_INIT,   3296,   2529,   2132,   1878,   1698,   1561,   1453,   1365,   1291,
    1228,   1173,   1125,   1083,   1045,   1010,   979,    951,    925,    901,
    878,    858,    838,    820,    803,    788,    773,    758,    745,    732,
    720,    709,    698,    687,    677,    667,    658,    649,    641,    633,
    625,    617,    610,    603,    596,    589,    583,    577,    571,    565,
    559,    554,    549,    544,    539,    534,    529,    524,    520,    515,
    511,    507,    503,    499,    495,    491,    487,    484,    480,    477,
    473,    470,    467,    464,    461,    457,    454,    452,    449,    446,
    443,    440,    438,    435,    432,    430,    427,    425,    422,    420,
    418,    415,    413,    411,    409,    407,    405,    402,    400,    398,
    396,    394,    393,    391,    389,    387,    385,    383,    382,    380,
    378,    376,    375,    373,    371,    370,    368,    367,    365,    364,
    362,    360,    359,    358,    356,    355,    353,    352,    351,    349
};
#endif

/* local function declalation */
void CheckMainMotor(void);

static void MotorTimerISR(void);

void MOTORPinMuxSetup(void);
void CoverPinMuxSetup(void);


/* function definition */
void ActuatorInit(void)
{
    /* main motor timer setup */
    IntRegister(SYS_INT_TINT34_1, MotorTimerISR);
    IntChannelSet(SYS_INT_TINT34_1, 12);
    IntSystemEnable(SYS_INT_TINT34_1);
    TimerConfigure(SOC_TMR_1_REGS, TMR_CFG_32BIT_UNCH_CLK_BOTH_INT);

    /* pin IO setup */
    MOTORPinMuxSetup();
	CoverPinMuxSetup();

    /* check main motor */
    CheckMainMotor();

    DBG_PRINT(" ");
}

void CheckMainMotor(void)
{
	MotorStart(MOTOR_SPD_MODE_NORMAL, MOTOR_DIRECTION_FORWARD);
	delay(1000);
	MotorEnd();

    DBG_PRINT(" ");
}

static void MotorTimerISR(void)
{
	/* generate motor clock */
	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_STEP, gClkPhase);

	if (gMotorSpdMode == MOTOR_SPD_MODE_NORMAL)
    {
		/* motor accelation */
        if ((gAccelTableIdx < MOTOR_TABLE_LENGTH) &&
            (gMotorTimerCnt > 2) &&
            ((gMotorTimerCnt % 2) == 0))
        {
            TimerDisable(SOC_TMR_1_REGS, TMR_TIMER34);
            TimerPeriodSet(SOC_TMR_1_REGS,
                           TMR_TIMER34,
                           MotorAccelTable[gAccelTableIdx] * MOTOR_FACT);
            TimerEnable(SOC_TMR_1_REGS, TMR_TIMER34, TMR_ENABLE_CONT);

            gAccelTableIdx++;
        }
	}

	gMotorTimerCnt++;
    gClkPhase ^= 0x01;

    /* clear the interrupt status in AINTC and in timer */
    IntSystemStatusClear(SYS_INT_TINT34_1);
    TimerIntStatusClear(SOC_TMR_1_REGS, TMR_INT_TMR34_NON_CAPT_MODE);
}

void MotorStart(unsigned int SpdMode, unsigned int Dir)
{
	gMotorSpdMode  = SpdMode;
	gMotorTimerCnt = 0;
	gAccelTableIdx = 0;

	if (SpdMode == MOTOR_SPD_MODE_NORMAL)
    {
        DBG_PRINT("normal mode");
		TimerPeriodSet(SOC_TMR_1_REGS, TMR_TIMER34, MOTOR_TIMER_PPS_INIT * MOTOR_FACT);
	}
    else if (SpdMode == MOTOR_SPD_MODE_BRAND)
    {
        DBG_PRINT("brand mode");
		TimerPeriodSet(SOC_TMR_1_REGS, TMR_TIMER34, MOTOR_TIMER_PPS_BRAND * MOTOR_FACT);
	}

    /* direction out */
	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_DIR, Dir);
	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_EN, GPIO_PIN_HIGH);

	/* motor timer reset */
	TimerCounterSet(SOC_TMR_1_REGS, TMR_TIMER34, 0x00000000);
	TimerIntEnable(SOC_TMR_1_REGS, TMR_INT_TMR34_NON_CAPT_MODE);
	TimerEnable(SOC_TMR_1_REGS, TMR_TIMER34, TMR_ENABLE_CONT);
}

void MotorEnd(void)
{
	TimerIntDisable(SOC_TMR_1_REGS, TMR_INT_TMR34_NON_CAPT_MODE);
	TimerDisable(SOC_TMR_1_REGS, TMR_TIMER34);

	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_EN, GPIO_PIN_LOW);
	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_STEP, GPIO_PIN_LOW);

    DBG_PRINT(" ");
}

unsigned int GetMotorTick(void)
{
    return gMotorTimerCnt >> 1;
}

void MotorONTest(unsigned int MotorType, unsigned int Direction)
{
	if (MotorType == MOTOR_TYPE_MAIN)
    {
		MotorStart(MOTOR_SPD_MODE_NORMAL, Direction);
	}
    else
    {
		if (Direction == MOTOR_DIRECTION_FORWARD)
        {
			BranderTphPosition();
		}
        else
        {
			BranderHomePosition();
		}
	}
}

void MotorOFFTest(unsigned int MotorType)
{
	if (MotorType == MOTOR_TYPE_MAIN)
    {
		MotorEnd();
	}
    else
    {
		/* do nothing */
	}
}

void MOTORPinMuxSetup(void)
{
    unsigned int PinMux;

    /* main motor pin mux setup */
    PinMux = HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(11)) &
                       ~(SYSCFG_PINMUX11_PINMUX11_7_4 |
                       SYSCFG_PINMUX11_PINMUX11_3_0);
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(11)) = (MOTOR_PIN_GROUP1 | PinMux);

    PinMux = HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(10)) &
                       ~(SYSCFG_PINMUX10_PINMUX10_31_28 |
                         SYSCFG_PINMUX10_PINMUX10_27_24 |
                         SYSCFG_PINMUX10_PINMUX10_23_20 |
                         SYSCFG_PINMUX10_PINMUX10_19_16 |
                         SYSCFG_PINMUX10_PINMUX10_15_12 |
                         SYSCFG_PINMUX10_PINMUX10_11_8 |
                         SYSCFG_PINMUX10_PINMUX10_7_4);
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(10)) = (MOTOR_PIN_GROUP2 | PinMux);

    PinMux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(4)) &
                        ~(SYSCFG_PINMUX4_PINMUX4_15_12));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(4)) = (MOTOR_PIN_GROUP3 | PinMux);

    /* main motor pin out initialize */
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_EN, GPIO_PIN_LOW);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_DIR, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_STEP, GPIO_PIN_LOW);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_SPD_SEL_1, GPIO_PIN_LOW);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_MAIN_MOT_SPD_SEL_2, GPIO_PIN_LOW);

    /* main motor pin direction initialize */
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_MAIN_MOT_EN, GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_MAIN_MOT_DIR, GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_MAIN_MOT_STEP, GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_MAIN_MOT_SPD_SEL_1, GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_MAIN_MOT_SPD_SEL_2, GPIO_DIR_OUTPUT);
}

void CoverPinMuxSetup(void)
{
    unsigned int PinMux;

    PinMux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(1)) &
                 ~(SYSCFG_PINMUX1_PINMUX1_7_4));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(1)) = (COVER_OPEN_PIN | PinMux);

    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_UP_COVER, GPIO_DIR_INPUT);
}
