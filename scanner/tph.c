#include <string.h>

#include "gpio.h"
#include "timer.h"
#include "interrupt.h"
#include "soc_AM1808.h"
#include "hw_syscfg0_AM1808.h"
#include "evmAM1808_uPP.h"
#include "flash.h"

#include "delay.h"
#include "sensor.h"
#include "actuator.h"
#include "tph.h"
#include "devcfg.h"

#include "debug.h"


/* font layout */
#define FONT_NUM_H_DOT					    (32)	// dots
#define FONT_NUM_V_DOT					    (48)	// dots
#define FONT_SIZE_BYTE					    (192)	// bytes
#define FONT_NUM_H_BYTE					    (FONT_NUM_H_DOT / BIT_PER_BYTE)		// bytes

/* font table from ascii code */
#define ASCII_CODE_OFFSET                   (45)

/* tph pin setup value */
#define MUX_HOME_SENSE_CFG                  (SYSCFG_PINMUX16_PINMUX16_31_28_GPIO7_10    << \
									         SYSCFG_PINMUX16_PINMUX16_31_28_SHIFT)
#define MUX_24V_CFG		                    (SYSCFG_PINMUX0_PINMUX0_19_16_GPIO0_11      << \
							                 SYSCFG_PINMUX0_PINMUX0_19_16_SHIFT)
#define MUX_LATCH_CFG                       (SYSCFG_PINMUX2_PINMUX2_23_20_GPIO1_10      << \
                                             SYSCFG_PINMUX2_PINMUX2_23_20_SHIFT)
#define MUX_STB_CFG                         (SYSCFG_PINMUX2_PINMUX2_19_16_GPIO1_11      << \
                                             SYSCFG_PINMUX2_PINMUX2_19_16_SHIFT)
#define MUX_5V_CFG                          (SYSCFG_PINMUX4_PINMUX4_31_28_GPIO1_0       << \
                                             SYSCFG_PINMUX4_PINMUX4_31_28_SHIFT)

/* pin number */
#define PIN_LATCH                           (27)
#define PIN_STROBE                          (28)
#define PIN_24V						        (12)
#define PIN_5V                              (13)
#define PIN_BRAND_MOT_CW			        (67)    // 시계 방향 (clock wise)
#define PIN_BRAND_MOT_CCW				    (68)    // 역시계 방향 (counter clock wise)
#define PIN_TPH_HOME_SENS				    (123)
#define PIN_BRAND_MOT_FORWARD_IN		    (67)
#define PIN_BRAND_MOT_REVERSE_IN		    (68)


static unsigned int gState;
static unsigned int g100usInterval;
static unsigned int gSendLineCnt;
static unsigned int gPrintLineCnt;

static unsigned int gImageSize;
static unsigned int gRemainSize;

#pragma data_alignment = 8
unsigned char	    gaByteBuf[TPH_BYTE_BUF_LENGTH];	        //OCR FONT중 입력된 char을 나열
unsigned char	    gaBitBuf[TPH_BIT_BUF_LENGTH];           //바이트단위로 정리된 폰트를 비트단위로 변경
static unsigned int gBitBufPtr;


/* local function declalation */
static void CheckBrandMotor(void);

static void TPHActivateStrobe(void);
static void TPHDeactivateStrobe(void);

static inline void TPHActivateLatch(void);
static inline void TPHDeactivateLatch(void);

static void TPHBrandStart(void);
static void TPHBrandEnd(void);

static void TPHTimerIsr(void);

static void TPHPinSetup(void);

static unsigned short TPHLoadImage(void);
static void TPHConvertBit2Byte(unsigned char *ByteBuf, unsigned char *BitBuf, int BitBufSize);


/* function definition */
unsigned short TPHInit(void)
{
    unsigned short ret;

    UPP_init();

    /* timer interrupt setup */
	IntRegister(SYS_INT_TINT34_0, TPHTimerIsr);
	IntChannelSet(SYS_INT_TINT34_0, 14);
	IntSystemEnable(SYS_INT_TINT34_0);
    TPHPinSetup();

    CheckBrandMotor();

    ret = TPHLoadImage();

    DBG_PRINT(" ");

    return ret;
}

int TPHGetImageInfo(unsigned short ImageSize)
{
    int numSector;
    int Cnt;

    if (((ImageSize % (DOT_PER_COLUM / BIT_PER_BYTE)) > 0) ||
        ((ImageSize / (DOT_PER_COLUM / BIT_PER_BYTE)) > DOT_PER_LOW) ||
        (ImageSize == 0))
    {
        DBG_PRINT("wrong image size");
        return TPH_RET_FAILURE;
    }

    gImageSize      = ImageSize;
    gRemainSize     = ImageSize;
    gPrintLineCnt   = ImageSize / (DOT_PER_COLUM / BIT_PER_BYTE);
    gBitBufPtr      = 0;

    FlashSectorErase(FALSH_BRAND_IMAGE_INFO_ADDR);
    DBG_PRINT("image info erase");

    numSector = (ImageSize * BIT_PER_BYTE) / FLASH_SECTOR_SIZE;
    if (((ImageSize * BIT_PER_BYTE) % FLASH_SECTOR_SIZE) > 0)
    {
        numSector++;
    }

    for (Cnt = 0; Cnt < numSector; Cnt++)
    {
        FlashSectorErase(FALSH_BRAND_IMAGE_DATA_ADDR + (Cnt * FLASH_SECTOR_SIZE));
        DBG_PRINT("image data sector %d erase", Cnt);
    }

    FlashWrite(FALSH_BRAND_IMAGE_INFO_ADDR, (unsigned char *)&ImageSize, BRAND_IMAGE_SIZE_LEN);

    return TPH_RET_SUCCESS;
}

int TPHGetImageData(unsigned char *pImage)
{
    int Div, Remain, Chunk;

    if (gRemainSize == 0)
    {
        return TPH_RET_FAILURE;
    }
    else if (gRemainSize >= TPH_DATA_CHUNK_LEN)
    {
        memcpy(&gaBitBuf[gBitBufPtr], pImage, TPH_DATA_CHUNK_LEN);
        gRemainSize -= TPH_DATA_CHUNK_LEN;
        gBitBufPtr  += TPH_DATA_CHUNK_LEN;

        if (gRemainSize == 0)
        {
            goto FLUSH;
        }
        else
        {
            return TPH_RET_WAIT;
        }
    }
    else
    {
        memcpy(&gaBitBuf[gBitBufPtr], pImage, gRemainSize);
        gRemainSize = 0;
    }

FLUSH:
    TPHConvertBit2Byte(gaByteBuf, gaBitBuf, gImageSize);

    Div    = (gImageSize * BIT_PER_BYTE) / MAX_FLASH_WRITE_SIZE;
    Remain = (gImageSize * BIT_PER_BYTE) % MAX_FLASH_WRITE_SIZE;

    for (Chunk = 0; Chunk < Div; Chunk++)
    {
        FlashWrite(FALSH_BRAND_IMAGE_DATA_ADDR + (Chunk * MAX_FLASH_WRITE_SIZE),
                   &gaByteBuf[Chunk * MAX_FLASH_WRITE_SIZE],
                   MAX_FLASH_WRITE_SIZE);
    }

    if (Remain > 0)
    {
        FlashWrite(FALSH_BRAND_IMAGE_DATA_ADDR + (Chunk * MAX_FLASH_WRITE_SIZE),
                   &gaByteBuf[Chunk * MAX_FLASH_WRITE_SIZE],
                   Remain);
    }

    return TPH_RET_SUCCESS;
}

static unsigned short TPHLoadImage(void)
{
    unsigned short  ImageSize;
    unsigned int    Div, Remain;
    unsigned int    Chunk;

    FlashRead(FALSH_BRAND_IMAGE_INFO_ADDR, (unsigned char *)&ImageSize, BRAND_IMAGE_SIZE_LEN);

    gImageSize      = ImageSize;
    gPrintLineCnt   = ImageSize / (DOT_PER_COLUM / BIT_PER_BYTE);

    if (gImageSize < TPH_BYTE_BUF_LENGTH)
    {
        ImageSize = gImageSize * BIT_PER_BYTE;

        Div = ImageSize / MAX_FLASH_READ_SIZE;
        Remain = ImageSize % MAX_FLASH_READ_SIZE;

        for (Chunk = 0; Chunk < Div; Chunk++)
        {
            FlashRead(FALSH_BRAND_IMAGE_DATA_ADDR + (Chunk * MAX_FLASH_READ_SIZE),
                &gaByteBuf[Chunk * MAX_FLASH_READ_SIZE],
                MAX_FLASH_READ_SIZE);
        }

        if (Remain > 0)
        {
            FlashRead(FALSH_BRAND_IMAGE_DATA_ADDR + (Chunk * MAX_FLASH_READ_SIZE),
                &gaByteBuf[Chunk * MAX_FLASH_READ_SIZE],
                Remain);
        }
    }

    return gImageSize;
}

/*
 * 비트맵으로 표현되어 있는 폰트를 도트당 바이트로 표현하는 함수
 * UPP라는 디바이스가 TPH에게 도트데이터를 전송해주는데 8bit bus interface를 사용한다.
 * 현재 TPH와 UPP는 0번 핀만 사용하기 때문에 도트 바이트는 0x00이 아니면 0x01의 값만 취한다.
 */
static void TPHConvertBit2Byte(unsigned char *ByteBuf, unsigned char *BitBuf, int BitBufSize)
{
    unsigned int    BitOffset;
    unsigned int    ByteBufOffset;
    unsigned int    BitBufOffset;
    unsigned int    Cnt;

    ByteBufOffset   = 0;
    BitBufOffset    = 0;
    Cnt             = BitBufSize * BIT_PER_BYTE;

    while (ByteBufOffset < Cnt)
    {
		for (BitOffset = 0; BitOffset < BIT_PER_BYTE; BitOffset++)
        {
            ByteBuf[ByteBufOffset] = (BitBuf[BitBufOffset] >> ((BIT_PER_BYTE - 1) - BitOffset)) & 0x01;
            ByteBufOffset++;
		}

        BitBufOffset++;
	}
}

void CheckBrandMotor(void)
{
	volatile int BMotorCnt;

    if (MaskSensorON(1 << SENS_TPH) > 0)
    {
		/* TPH -> HOME */
        DBG_PRINT("TPH -> HOME\r\n");
		BranderHomePosition();
	}
    else
    {
		/* HOME -> TPH -> HOME */
        DBG_PRINT("HOME -> TPH -> HOME\r\n");
		BranderTphPosition();
		BranderHomePosition();
	}
}

void BranderHomePosition(void)
{
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CCW, GPIO_PIN_LOW);
    while (MaskSensorON(1 << SENS_TPH) > 0);
    delay(200);

    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CCW, GPIO_PIN_HIGH);
}

void BranderTphPosition(void)
{
    /** 1. TPH Position Check **/
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CW, GPIO_PIN_LOW);
    while (MaskSensorON(1 << SENS_TPH) == 0);
    delay(101);

	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CW, GPIO_PIN_HIGH);
    delay(5);
}

/*
 * Periode 100us
 * 1. 폰트데이터 전송
 * 2. nLATCH 신호 발생
 * 3. nSTROBE 신호 발생 (500us, 주의! 더 길어질 경우 TPH가 탈 수 있음)
 */
static void TPHTimerIsr(void)
{
    unsigned int Offset;

    TimerIntDisable(SOC_TMR_0_REGS, TMR_INT_TMR34_NON_CAPT_MODE);

    /* Clear the interrupt status in AINTC and in timer */
    IntSystemStatusClear(SYS_INT_TINT34_0);
    TimerIntStatusClear(SOC_TMR_0_REGS, TMR_INT_TMR34_NON_CAPT_MODE);

    if (g100usInterval == 0)
    {
        /* transmit dot line data */
        Offset = ((unsigned int)(&gaByteBuf[gSendLineCnt * DOT_PER_COLUM]));
        SendUppToPLD(Offset, DOT_PER_COLUM);
    }
    else if (g100usInterval == 1)
    {
        TPHActivateLatch();
    }
    else if (g100usInterval == 2)
    {
        TPHDeactivateLatch();
    }
    else if (g100usInterval == 3)
    {
        TPHActivateStrobe();
    }
    else if (g100usInterval == 8)
    {
        TPHDeactivateStrobe();
    }

    if (g100usInterval == 8)
    {
        gSendLineCnt++;
        if (gSendLineCnt == gPrintLineCnt)
        {
           gState = TPH_DONE;
        }

        g100usInterval = 0;
    }
    else
    {
        g100usInterval++;
    }

    /* Enable the timer interrupt */
    TimerIntEnable(SOC_TMR_0_REGS, TMR_INT_TMR34_NON_CAPT_MODE);
}

void TPHBrand(void)
{
    gState          = TPH_START;
    g100usInterval  = 0;
    gSendLineCnt    = 0;

    /** TPH Timer -- 100usec**/
    TimerDisable(SOC_TMR_0_REGS, TMR_TIMER34);
    TimerPeriodSet(SOC_TMR_0_REGS, TMR_TIMER34, 48 * 100);
    TimerCounterSet(SOC_TMR_0_REGS, TMR_TIMER34, 0x00000000);
    TimerIntEnable(SOC_TMR_0_REGS, TMR_INT_TMR34_NON_CAPT_MODE);
    TimerEnable(SOC_TMR_0_REGS, TMR_TIMER34, TMR_ENABLE_CONT);

    TPHBrandStart();
    /* wait busy */
    while (gState != TPH_DONE);
    TPHBrandEnd();

    TimerDisable(SOC_TMR_0_REGS, TMR_TIMER34);

    gState = TPH_IDLE;

    DBG_PRINT("Brand %d line done", gSendLineCnt);
}

// Strobe On/Off
static inline void TPHActivateStrobe(void)
{
	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_STROBE, GPIO_PIN_LOW);
}

static inline void TPHDeactivateStrobe(void)
{
	GPIOPinWrite(SOC_GPIO_0_REGS, PIN_STROBE, GPIO_PIN_HIGH);
}

static inline void TPHActivateLatch(void)
{
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_LATCH, GPIO_PIN_LOW);
}

static inline void TPHDeactivateLatch(void)
{
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_LATCH, GPIO_PIN_HIGH);
}

static void TPHBrandStart(void)
{
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_5V, GPIO_PIN_HIGH);
    delay(1);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_24V, GPIO_PIN_HIGH);
}

static void TPHBrandEnd(void)
{
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_24V, GPIO_PIN_LOW);
    delay(1);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_5V, GPIO_PIN_LOW);
}

static void TPHPinSetup(void)
{
    unsigned int savePinmux;

    /* brand motor pin direction */
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CW, GPIO_DIR_OUTPUT);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CCW, GPIO_DIR_OUTPUT);

    /* brand motor init value */
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CW, GPIO_PIN_HIGH);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_BRAND_MOT_CCW, GPIO_PIN_HIGH);

    /* tph home sensor mux setup */
    savePinmux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(16)) &
                        ~(SYSCFG_PINMUX16_PINMUX16_31_28));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(16)) = (MUX_HOME_SENSE_CFG | savePinmux);

    /* tph home sensor initial value */
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_TPH_HOME_SENS, GPIO_DIR_INPUT);

    /* tph on (gp0.11) mux setup */
    savePinmux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(0)) &
                        ~(SYSCFG_PINMUX0_PINMUX0_19_16));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(0)) = (MUX_24V_CFG | savePinmux);

    /* supply voltage control pin setup */
    savePinmux = (HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(0)) &
                        ~(SYSCFG_PINMUX0_PINMUX0_15_12));
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(0)) = (MUX_5V_CFG | savePinmux);
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_5V, GPIO_DIR_OUTPUT);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_5V, GPIO_PIN_LOW);

    /* tph on initial value */
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_24V, GPIO_DIR_OUTPUT);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_24V, GPIO_PIN_LOW);

    /* latch (gp1.11) strobe (gp1.10) mux setup */
    savePinmux = HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(2)) &
                        ~(SYSCFG_PINMUX2_PINMUX2_23_20 |
                          SYSCFG_PINMUX2_PINMUX2_19_16);
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(2)) = (MUX_LATCH_CFG | MUX_STB_CFG | savePinmux);

    /* latch, strobe initial value */
    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_LATCH, GPIO_DIR_OUTPUT);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_LATCH, GPIO_PIN_HIGH);

    GPIODirModeSet(SOC_GPIO_0_REGS, PIN_STROBE, GPIO_DIR_OUTPUT);
    GPIOPinWrite(SOC_GPIO_0_REGS, PIN_STROBE, GPIO_PIN_HIGH);
}