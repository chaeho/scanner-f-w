//-----------------------------------------------------------------------------
// \file	evmam1808_uPP.c
// \brief   implementation of uPP driver for AM-1808.
//
//-----------------------------------------------------------------------------


#include "stdio.h"
#include "hw_types.h"
#include "soc_AM1808.h"
#include "hw_psc_AM1808.h"
#include "hw_syscfg0_AM1808.h"
#include "psc.h"
#include "Hw_mcbsp.h"
#include "evmAM1808_uPP.h"
#include "interrupt.h"
//-----------------------------------------------------------------------------
// Private Defines
//-----------------------------------------------------------------------------
#define PINMUX_UPP_REG_0	(19)
#define PINMUX_UPP_MASK_0	(0x000F0000)
#define PINMUX_UPP_VAL_0	(0x00010000)

#define PINMUX_UPP_REG_1	(13)
#define PINMUX_UPP_MASK_1	(0xFFFF0000)
#define PINMUX_UPP_VAL_1	(0x48440000)

#define PINMUX_UPP_REG_2	(14)
#define PINMUX_UPP_MASK_2	(0xFFFFFF00)
#define PINMUX_UPP_VAL_2	(0x44444400)

#define PINMUX_UPP_REG_3	(15)
#define PINMUX_UPP_MASK_3	(0xFFFFFFFF)
#define PINMUX_UPP_VAL_3	(0x44444444)

#define PINMUX_UPP_REG_4	(16)
#define PINMUX_UPP_MASK_4	(0xFFFFFFFF)
#define PINMUX_UPP_VAL_4	(0x44444444)

#define PINMUX_UPP_REG_5	(17)
#define PINMUX_UPP_MASK_5	(0xFFFFFFFF)
#define PINMUX_UPP_VAL_5	(0x44444444)

#define PINMUX_UPP_REG_6	(15)
#define PINMUX_UPP_MASK_6	(0x00FFFFFF)
#define PINMUX_UPP_VAL_6	(0x00444444)

#define PINMUX_UPP_REG_7	(18)
#define PINMUX_UPP_MASK_7	(0x00FFFFFF)
#define PINMUX_UPP_VAL_7	(0x00444444)

#define PINMUX2_MCBSP0_CLKX0_ENABLE	(SYSCFG_PINMUX2_PINMUX2_11_8_CLKX0 <<\
										SYSCFG_PINMUX2_PINMUX2_11_8_SHIFT)

#define PINMUX2_MCBSP0_DX0_ENABLE	(SYSCFG_PINMUX2_PINMUX2_27_24_DX0 << \
										SYSCFG_PINMUX2_PINMUX2_27_24_SHIFT)

#define PINMUX2_MCBSP0_FSX0_ENABLE	(SYSCFG_PINMUX2_PINMUX2_19_16_FSX0 << \
										SYSCFG_PINMUX2_PINMUX2_19_16_SHIFT)



//-----------------------------------------------------------------------------
// Public Function Definitions
//-----------------------------------------------------------------------------
void uPPPinMuxSetup(void)
{
    unsigned int savePinmux;

    /* clock, start, enable pin set */
    savePinmux = HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(13)) &
				            ~(SYSCFG_PINMUX13_PINMUX13_19_16 |
						      SYSCFG_PINMUX13_PINMUX13_23_20 |
						      SYSCFG_PINMUX13_PINMUX13_27_24 |
                              SYSCFG_PINMUX13_PINMUX13_31_28);
	HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(13)) = (uPPConfigPin | savePinmux);

    /* data 0 pin set */
    savePinmux = HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(16)) &
				               ~(SYSCFG_PINMUX16_PINMUX16_7_4);
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(16)) = (uPPDataPin | savePinmux);
}
//-----------------------------------------------------------------------------
// \brief   initialize the upp port
//
// \param   upp_config_t * config: structure containing nessisary information
//									to setup upp
//
// \return  uint32_t
//	ERR_NO_ERROR - everything is ok...upp ready to use.
//-----------------------------------------------------------------------------
void UPP_init(void)
{
	upp_config_t	config;
	unsigned int	i;

	//UPCTL page31
	config.UPCTL.value = 0;				//spec page31
	//config.UPCTL.bits.IWB = 0;			//0:8 bit interface 1:16bint interface
	//config.UPCTL.bits.DPWB = 0;		//8 bit data

	config.UPCTL.bits.DPFA = 0;
	config.UPCTL.bits.DPWA = 0;	//no data packing(8bit or 16bit)
	config.UPCTL.bits.IWA = 0;		//channel A 8bit interface

	config.UPCTL.bits.CHN = 0;			//0:single channel mode(only CH.A),1:dual mode(chA,chB both active)
	config.UPCTL.bits.MODE = 1;			//0 all recv, 1 all xmit, 2 a recv b xmit, 3 a xmit b recv
									   //Channel A ADC, Channel B DAC
	/*-----------------------------------------------------------------------------------------------------------
												Word Rate (Mw/s)
	UPICR.CLKDIVn	I/O Clock (MHz)   Single Data Rate	Double Data Rate
	0				75.00		75.00			...		<---파형이 아날로그처럼 나옴
	1			37.50			37.50					75.00	<---파형 괜찮음
	2				25.00			25.00				50.00
	3				18.75			18.75				37.50
	...			...			...				...
	15				4.69			4.69				9.38
	-------------------------------------------------------------------------------------------------------------*/
	//UPICR page32
	config.UPICR.value = 0;
	config.UPICR.bits.CLKDIVA = 9;		//Set DAC sampling freqency at 75/10 Mhz (7.5MHz)
	//config->UPICR.bits.CLKDIVA = 0;		//Set DAC sampling freqency at 75/1 Mhz (75MHz)

	//UPIVR page34
	config.UPIVR.value = 0;
	//config.UPIVR.bits.VALB = 0x1000;
	config.UPIVR.bits.VALA = 0x0000;	//idle인 동안 데이터라인 값 0x00으로 한다.

	//UPTCR page35
	config.UPTCR.value = 0;				//all values 0 for 64byte DMA bursts read / write
	//config->UPTCR.bits.RDSIZEI=0;		//channel I readsize 0:64,1:128,3:256 byte
	//UPDLB
	config.UPDLB.value = 0;				//no loopback
	//UPIES
	config.UPIES.value = 0;				//dont enable any interrupts
	//UPPCR
	config.UPPCR.value = 0;
	config.UPPCR.bits.EN = 1;			//enable uPP
	config.UPPCR.bits.RTEMU = 1;		//allow emulator use
	config.UPPCR.bits.SOFT = 1;			//allow emulation

	PSCModuleControl(SOC_PSC_1_REGS, HW_PSC_UPP, PSC_POWERDOMAIN_ALWAYS_ON, PSC_MDCTL_NEXT_ENABLE);

    uPPPinMuxSetup();

	//reset uPP
	SETBIT(UPP->UPPCR, UPP_UPPCR_SWRST);
	for (i = 0; i < 300; i++);		//wait 200 clock cycles for reset.
	CLRBIT(UPP->UPPCR, UPP_UPPCR_SWRST);

	//setup control registers
	UPP->UPCTL = config.UPCTL.value;
	UPP->UPICR = config.UPICR.value;
	UPP->UPIVR = config.UPIVR.value;
	UPP->UPTCR = config.UPTCR.value;
	UPP->UPDLB = config.UPDLB.value;
	UPP->UPIES = config.UPIES.value;
	UPP->UPPCR = config.UPPCR.value;
}
void SendUppToPLD(unsigned int BufAddr, unsigned int Length)
{
	UPP->UPID0 = (unsigned int)BufAddr;
	//UPP->UPID1 = 0x00010200;   			//1 lines (512byte)
    //UPP->UPID1 = 0x00010100;   			//1 lines (256byte)
    UPP->UPID1 = 0x00010000 | Length;
    UPP->UPID2 = 0x00000000;
}
void MCBSP_Init()
{
    unsigned int savePinMux = 0;
    /* Power up the McASP module */
    PSCModuleControl(SOC_PSC_1_REGS, HW_PSC_MCBSP0, PSC_POWERDOMAIN_ALWAYS_ON, PSC_MDCTL_NEXT_ENABLE);

    /*
     ** Clearing the bits in context and retaining the other bit values
     ** of PINMUX0 register.
     */
    savePinMux = HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(2)) &
					        ~(SYSCFG_PINMUX2_PINMUX2_27_24 |
            				  SYSCFG_PINMUX2_PINMUX2_11_8 |
					          SYSCFG_PINMUX2_PINMUX2_19_16 );

    /*
    ** Performing the actual Pin Multiplexing to select mandatory pins in
    ** PINMUX0  register.
    */
    HWREG(SOC_SYSCFG_0_REGS + SYSCFG0_PINMUX(2)) = \
    (PINMUX2_MCBSP0_CLKX0_ENABLE | PINMUX2_MCBSP0_DX0_ENABLE | \
    PINMUX2_MCBSP0_FSX0_ENABLE | savePinMux);

    //gobin 적용
    HWREG(SOC_MCBSP_0_CTRL_REGS + 0x10) = 0x7f057f00;//
    HWREG(SOC_MCBSP_0_CTRL_REGS + 0x14) = 0x3fff0095;//frame 4096 bit period, 15mhz
    HWREG(SOC_MCBSP_0_CTRL_REGS + 0x24) = 0x00000e00; //pcr reg.
    HWREG(SOC_MCBSP_0_CTRL_REGS + 0x08) |= 0x02000000;
    HWREG(SOC_MCBSP_0_CTRL_REGS + 0x08) |= 0x00000000;//xintm (new frame)200000
    HWREG(SOC_MCBSP_0_CTRL_REGS + 0x08) |= 0x00400000;//grst =1
}
void mcbsp0_write(unsigned char out_data)
{
    HWREG(SOC_MCBSP_0_CTRL_REGS + MCBSP_DXR) = out_data;
}


