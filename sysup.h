#ifndef __SYSUP_H__
#define __SYSUP_H__

void PSCModuleEnable(void);

void CopyVectorTable(void);
void SetupIntc(void);
void SDRAMInit(void);

#endif
