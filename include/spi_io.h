#ifndef __SPI_IO_H__
#define __SPI_IO_H__

#include "spi.h"

#define SPI_IO_FLAG_NO_RESPONSE         (SPI_TRANSMIT_INT)
#define SPI_IO_FLAG_RW                  (SPI_TRANSMIT_INT | SPI_RECV_INT)

#define SPI0_DEVICE_ID_ADC              (0)
#define SPI0_DEVICE_ID_AFE              (1)


void SetUpSPI0(void);
void SetUpSPI1(void);

unsigned int SPI0Transfer16b(unsigned int DeviceNum, unsigned int Data, unsigned int Flag);

#endif