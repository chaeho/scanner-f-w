//-----------------------------------------------------------------------------
// \file	evmam1808_uPP.h
// \brief   AM-1808 uPP registers, bit definitions, and
//		function prototypes.
//
//-----------------------------------------------------------------------------

#ifndef EVMAM1808_UPP_H
#define EVMAM1808_UPP_H

//-----------------------------------------------------------------------------
// Register Structure & Defines
//-----------------------------------------------------------------------------

//bits UPPCR

/**************************************************************************\
* Field Definition Macros
\**************************************************************************/

/* UPPID */

#define UPP_UPPID_REVID		(0xFFFFFFFFu)
#define UPP_UPPID_REVID_SHIFT		(0x00000000u)


/* UPPCR */


#define UPP_UPPCR_DB			(0x00000080u)
#define UPP_UPPCR_DB_SHIFT				(0x00000007u)

#define UPP_UPPCR_SWRST		(0x00000010u)
#define UPP_UPPCR_SWRST_SHIFT		(0x00000004u)

#define UPP_UPPCR_EN			(0x00000008u)
#define UPP_UPPCR_EN_SHIFT		(0x00000003u)

#define UPP_UPPCR_RTEMU		(0x00000004u)
#define UPP_UPPCR_RTEMU_SHIFT		(0x00000002u)

#define UPP_UPPCR_SOFT		(0x00000002u)
#define UPP_UPPCR_SOFT_SHIFT		(0x00000001u)

#define UPP_UPPCR_FREE		(0x00000001u)
#define UPP_UPPCR_FREE_SHIFT		(0x00000000u)


/* UPDLB */


#define UPP_UPDLB_BA			(0x00002000u)
#define UPP_UPDLB_BA_SHIFT		(0x0000000Du)

#define UPP_UPDLB_AB			(0x00001000u)
#define UPP_UPDLB_AB_SHIFT		(0x0000000Cu)

/* UPCTL */


#define UPP_UPCTL_DPFB		(0x60000000u)
#define UPP_UPCTL_DPFB_SHIFT		(0x0000001Du)
/*----DPFB Tokens----*/
#define UPP_UPCTL_DPFB_RJZE		(0x00000000u)
#define UPP_UPCTL_DPFB_RJSE		(0x00000001u)
#define UPP_UPCTL_DPFB_LJZF		(0x00000002u)
#define UPP_UPCTL_DPFB_RESERVED	(0x00000003u)

#define UPP_UPCTL_DPWB		(0x1C000000u)
#define UPP_UPCTL_DPWB_SHIFT		(0x0000001Au)
/*----DPWB Tokens----*/
#define UPP_UPCTL_DPWB_FULL		(0x00000000u)
#define UPP_UPCTL_DPWB_9BIT		(0x00000001u)
#define UPP_UPCTL_DPWB_10BIT		(0x00000002u)
#define UPP_UPCTL_DPWB_11BIT		(0x00000003u)
#define UPP_UPCTL_DPWB_12BIT		(0x00000004u)
#define UPP_UPCTL_DPWB_13BIT		(0x00000005u)
#define UPP_UPCTL_DPWB_14BIT		(0x00000006u)
#define UPP_UPCTL_DPWB_15BIT		(0x00000007u)

#define UPP_UPCTL_IWB		(0x02000000u)
#define UPP_UPCTL_IWB_SHIFT		(0x00000019u)

#define UPP_UPCTL_DRB		(0x01000000u)
#define UPP_UPCTL_DRB_SHIFT		(0x00000018u)

#define UPP_UPCTL_DPFA		(0x00600000u)
#define UPP_UPCTL_DPFA_SHIFT		(0x00000015u)
/*----DPFA Tokens----*/
#define UPP_UPCTL_DPFA_RJZE		(0x00000000u)
#define UPP_UPCTL_DPFA_RJSE		(0x00000001u)
#define UPP_UPCTL_DPFA_LJZF		(0x00000002u)
#define UPP_UPCTL_DPFA_RESERVED	(0x00000003u)

#define UPP_UPCTL_DPWA		(0x001C0000u)
#define UPP_UPCTL_DPWA_SHIFT		(0x00000012u)
/*----DPWA Tokens----*/
#define UPP_UPCTL_DPWA_FULL		(0x00000000u)
#define UPP_UPCTL_DPWA_9BIT		(0x00000001u)
#define UPP_UPCTL_DPWA_10BIT		(0x00000002u)
#define UPP_UPCTL_DPWA_11BIT		(0x00000003u)
#define UPP_UPCTL_DPWA_12BIT		(0x00000004u)
#define UPP_UPCTL_DPWA_13BIT		(0x00000005u)
#define UPP_UPCTL_DPWA_14BIT		(0x00000006u)
#define UPP_UPCTL_DPWA_15BIT		(0x00000007u)

#define UPP_UPCTL_IWA		(0x00020000u)
#define UPP_UPCTL_IWA_SHIFT		(0x00000011u)

#define UPP_UPCTL_DRA		(0x00010000u)
#define UPP_UPCTL_DRA_SHIFT		(0x00000010u)


#define UPP_UPCTL_DDRDEMUX	(0x00000010u)
#define UPP_UPCTL_DDRDEMUX_SHIFT	(0x00000004u)

#define UPP_UPCTL_SDRTXIL	(0x00000008u)
#define UPP_UPCTL_SDRTXIL_SHIFT	(0x00000003u)

#define UPP_UPCTL_CHN		(0x00000004u)
#define UPP_UPCTL_CHN_SHIFT		(0x00000002u)

#define UPP_UPCTL_MODE		(0x00000003u)
#define UPP_UPCTL_MODE_SHIFT		(0x00000000u)
/*----MODE Tokens----*/
#define UPP_UPCTL_MODE_RECEIVE	(0x00000000u)
#define UPP_UPCTL_MODE_TRANSMIT	(0x00000001u)
#define UPP_UPCTL_MODE_DUPLEX0	(0x00000002u)
#define UPP_UPCTL_MODE_DUPLEX1	(0x00000003u)


/* UPICR */


#define UPP_UPICR_TRISB		(0x20000000u)
#define UPP_UPICR_TRISB_SHIFT		(0x0000001Du)

#define UPP_UPICR_CLKINVB	(0x10000000u)
#define UPP_UPICR_CLKINVB_SHIFT	(0x0000001Cu)

#define UPP_UPICR_CLKDIVB	(0x0F000000u)
#define UPP_UPICR_CLKDIVB_SHIFT	(0x00000018u)


#define UPP_UPICR_WAITB		(0x00200000u)
#define UPP_UPICR_WAITB_SHIFT		(0x00000015u)

#define UPP_UPICR_ENAB		(0x00100000u)
#define UPP_UPICR_ENAB_SHIFT		(0x00000014u)

#define UPP_UPICR_STARTB		(0x00080000u)
#define UPP_UPICR_STARTB_SHIFT	(0x00000013u)

#define UPP_UPICR_WAITPOLB	(0x00040000u)
#define UPP_UPICR_WAITPOLB_SHIFT	(0x00000012u)

#define UPP_UPICR_ENAPOLB	(0x00020000u)
#define UPP_UPICR_ENAPOLB_SHIFT	(0x00000011u)

#define UPP_UPICR_STARTPOLB	(0x00010000u)
#define UPP_UPICR_STARTPOLB_SHIFT	(0x00000010u)


#define UPP_UPICR_TRISA		(0x00002000u)
#define UPP_UPICR_TRISA_SHIFT		(0x0000000Du)

#define UPP_UPICR_CLKINVA	(0x00001000u)
#define UPP_UPICR_CLKINVA_SHIFT	(0x0000000Cu)

#define UPP_UPICR_CLKDIVA	(0x00000F00u)
#define UPP_UPICR_CLKDIVA_SHIFT	(0x00000008u)


#define UPP_UPICR_WAITA		(0x00000020u)
#define UPP_UPICR_WAITA_SHIFT		(0x00000005u)

#define UPP_UPICR_ENAA		(0x00000010u)
#define UPP_UPICR_ENAA_SHIFT		(0x00000004u)

#define UPP_UPICR_STARTA		(0x00000008u)
#define UPP_UPICR_STARTA_SHIFT	(0x00000003u)

#define UPP_UPICR_WAITPOLA	(0x00000004u)
#define UPP_UPICR_WAITPOLA_SHIFT	(0x00000002u)

#define UPP_UPICR_ENAPOLA	(0x00000002u)
#define UPP_UPICR_ENAPOLA_SHIFT	(0x00000001u)

#define UPP_UPICR_STARTPOLA	(0x00000001u)
#define UPP_UPICR_STARTPOLA_SHIFT	(0x00000000u)


/* UPIVR */

#define UPP_UPIVR_VALB		(0xFFFF0000u)
#define UPP_UPIVR_VALB_SHIFT		(0x00000010u)

#define UPP_UPIVR_VALA		(0x0000FFFFu)
#define UPP_UPIVR_VALA_SHIFT		(0x00000000u)


/* UPTCR */


#define UPP_UPTCR_TXSIZEB	(0x03000000u)
#define UPP_UPTCR_TXSIZEB_SHIFT	(0x00000018u)
/*----TXSIZEB Tokens----*/
#define UPP_UPTCR_TXSIZEB_64B		(0x00000000u)
#define UPP_UPTCR_TXSIZEB_128B	(0x00000001u)
#define UPP_UPTCR_TXSIZEB_256B	(0x00000003u)


#define UPP_UPTCR_TXSIZEA	(0x00030000u)
#define UPP_UPTCR_TXSIZEA_SHIFT	(0x00000010u)
/*----TXSIZEA Tokens----*/
#define UPP_UPTCR_TXSIZEA_64B		(0x00000000u)
#define UPP_UPTCR_TXSIZEA_128B	(0x00000001u)
#define UPP_UPTCR_TXSIZEA_256B	(0x00000003u)


#define UPP_UPTCR_RDSIZEQ	(0x00000300u)
#define UPP_UPTCR_RDSIZEQ_SHIFT	(0x00000008u)
/*----RDSIZEQ Tokens----*/
#define UPP_UPTCR_RDSIZEQ_64B		(0x00000000u)
#define UPP_UPTCR_RDSIZEQ_128B	(0x00000001u)
#define UPP_UPTCR_RDSIZEQ_256B	(0x00000003u)


#define UPP_UPTCR_RDSIZEI	(0x00000003u)
#define UPP_UPTCR_RDSIZEI_SHIFT	(0x00000000u)
/*----RDSIZEI Tokens----*/
#define UPP_UPTCR_RDSIZEI_64B		(0x00000000u)
#define UPP_UPTCR_RDSIZEI_128B	(0x00000001u)
#define UPP_UPTCR_RDSIZEI_256B	(0x00000003u)


/* UPISR */


#define UPP_UPISR_EOLQ		(0x00001000u)
#define UPP_UPISR_EOLQ_SHIFT		(0x0000000Cu)

#define UPP_UPISR_EOWQ		(0x00000800u)
#define UPP_UPISR_EOWQ_SHIFT		(0x0000000Bu)

#define UPP_UPISR_ERRQ		(0x00000400u)
#define UPP_UPISR_ERRQ_SHIFT		(0x0000000Au)

#define UPP_UPISR_UORQ		(0x00000200u)
#define UPP_UPISR_UORQ_SHIFT		(0x00000009u)

#define UPP_UPISR_DPEQ		(0x00000100u)
#define UPP_UPISR_DPEQ_SHIFT		(0x00000008u)

#define UPP_UPISR_EOLI		(0x00000010u)
#define UPP_UPISR_EOLI_SHIFT		(0x00000004u)

#define UPP_UPISR_EOWI		(0x00000008u)
#define UPP_UPISR_EOWI_SHIFT		(0x00000003u)

#define UPP_UPISR_ERRI		(0x00000004u)
#define UPP_UPISR_ERRI_SHIFT		(0x00000002u)

#define UPP_UPISR_UORI		(0x00000002u)
#define UPP_UPISR_UORI_SHIFT		(0x00000001u)

#define UPP_UPISR_DPEI		(0x00000001u)
#define UPP_UPISR_DPEI_SHIFT		(0x00000000u)


/* UPIER */


#define UPP_UPIER_EOLQ		(0x00001000u)
#define UPP_UPIER_EOLQ_SHIFT		(0x0000000Cu)

#define UPP_UPIER_EOWQ		(0x00000800u)
#define UPP_UPIER_EOWQ_SHIFT		(0x0000000Bu)
#define UPP_UPIER_EOWQ_TRUE		(0x00000001u)

#define UPP_UPIER_ERRQ		(0x00000400u)
#define UPP_UPIER_ERRQ_SHIFT		(0x0000000Au)

#define UPP_UPIER_UORQ		(0x00000200u)
#define UPP_UPIER_UORQ_SHIFT		(0x00000009u)
#define UPP_UPIER_UORQ_TRUE		(0x00000001u)

#define UPP_UPIER_DPEQ		(0x00000100u)
#define UPP_UPIER_DPEQ_SHIFT		(0x00000008u)


#define UPP_UPIER_EOLI		(0x00000010u)
#define UPP_UPIER_EOLI_SHIFT		(0x00000004u)

#define UPP_UPIER_EOWI		(0x00000008u)
#define UPP_UPIER_EOWI_SHIFT		(0x00000003u)

#define UPP_UPIER_ERRI		(0x00000004u)
#define UPP_UPIER_ERRI_SHIFT		(0x00000002u)

#define UPP_UPIER_UORI		(0x00000002u)
#define UPP_UPIER_UORI_SHIFT		(0x00000001u)

#define UPP_UPIER_DPEI		(0x00000001u)
#define UPP_UPIER_DPEI_SHIFT		(0x00000000u)


/* UPIES */


#define UPP_UPIES_EOLQ		(0x00001000u)
#define UPP_UPIES_EOLQ_SHIFT		(0x0000000Cu)

#define UPP_UPIES_EOWQ		(0x00000800u)
#define UPP_UPIES_EOWQ_SHIFT		(0x0000000Bu)

#define UPP_UPIES_ERRQ		(0x00000400u)
#define UPP_UPIES_ERRQ_SHIFT		(0x0000000Au)

#define UPP_UPIES_UORQ		(0x00000200u)
#define UPP_UPIES_UORQ_SHIFT		(0x00000009u)

#define UPP_UPIES_DPEQ		(0x00000100u)
#define UPP_UPIES_DPEQ_SHIFT		(0x00000008u)


#define UPP_UPIES_EOLI		(0x00000010u)
#define UPP_UPIES_EOLI_SHIFT		(0x00000004u)

#define UPP_UPIES_EOWI		(0x00000008u)
#define UPP_UPIES_EOWI_SHIFT		(0x00000003u)

#define UPP_UPIES_ERRI		(0x00000004u)
#define UPP_UPIES_ERRI_SHIFT		(0x00000002u)

#define UPP_UPIES_UORI		(0x00000002u)
#define UPP_UPIES_UORI_SHIFT		(0x00000001u)

#define UPP_UPIES_DPEI		(0x00000001u)
#define UPP_UPIES_DPEI_SHIFT		(0x00000000u)


/* UPIEC */


#define UPP_UPIEC_EOLQ		(0x00001000u)
#define UPP_UPIEC_EOLQ_SHIFT		(0x0000000Cu)

#define UPP_UPIEC_EOWQ		(0x00000800u)
#define UPP_UPIEC_EOWQ_SHIFT		(0x0000000Bu)

#define UPP_UPIEC_ERRQ		(0x00000400u)
#define UPP_UPIEC_ERRQ_SHIFT		(0x0000000Au)

#define UPP_UPIEC_UORQ		(0x00000200u)
#define UPP_UPIEC_UORQ_SHIFT		(0x00000009u)

#define UPP_UPIEC_DPEQ		(0x00000100u)
#define UPP_UPIEC_DPEQ_SHIFT		(0x00000008u)


#define UPP_UPIEC_EOLI		(0x00000010u)
#define UPP_UPIEC_EOLI_SHIFT		(0x00000004u)

#define UPP_UPIEC_EOWI		(0x00000008u)
#define UPP_UPIEC_EOWI_SHIFT		(0x00000003u)

#define UPP_UPIEC_ERRI		(0x00000004u)
#define UPP_UPIEC_ERRI_SHIFT		(0x00000002u)

#define UPP_UPIEC_UORI		(0x00000002u)
#define UPP_UPIEC_UORI_SHIFT		(0x00000001u)

#define UPP_UPIEC_DPEI		(0x00000001u)
#define UPP_UPIEC_DPEI_SHIFT		(0x00000000u)


/* UPEOI */


#define UPP_UPEOI_EOI		(0x000000FFu)
#define UPP_UPEOI_EOI_SHIFT		(0x00000000u)


/* UPID0 */

#define UPP_UPID0_ADDRH		(0xFFFFFFF8u)
#define UPP_UPID0_ADDRH_SHIFT		(0x00000003u)

#define UPP_UPID0_ADDR		(0x00000007u)
#define UPP_UPID0_ADDR_SHIFT		(0x00000000u)


/* UPID1 */

#define UPP_UPID1_LNCNT		(0xFFFF0000u)
#define UPP_UPID1_LNCNT_SHIFT		(0x00000010u)

#define UPP_UPID1_BCNTH		(0x0000FFFEu)
#define UPP_UPID1_BCNTH_SHIFT		(0x00000001u)

#define UPP_UPID1_BCNT		(0x00000001u)
#define UPP_UPID1_BCNT_SHIFT		(0x00000000u)


/* UPID2 */


#define UPP_UPID2_LNOFFSETH	(0x0000FFF8u)
#define UPP_UPID2_LNOFFSETH_SHIFT	(0x00000003u)

#define UPP_UPID2_LNOFFSET	(0x00000007u)
#define UPP_UPID2_LNOFFSET_SHIFT	(0x00000000u)


/* UPIS0 */

#define UPP_UPIS0_ADDR		(0xFFFFFFFFu)
#define UPP_UPIS0_ADDR_SHIFT		(0x00000000u)


/* UPIS1 */

#define UPP_UPIS1_LNCNT		(0xFFFF0000u)
#define UPP_UPIS1_LNCNT_SHIFT		(0x00000010u)

#define UPP_UPIS1_BCNT		(0x0000FFFFu)
#define UPP_UPIS1_BCNT_SHIFT		(0x00000000u)


/* UPIS2 */


#define UPP_UPIS2_WM			(0x000000F0u)
#define UPP_UPIS2_WM_SHIFT		(0x00000004u)


#define UPP_UPIS2_PEND		(0x00000002u)
#define UPP_UPIS2_PEND_SHIFT		(0x00000001u)

#define UPP_UPIS2_ACT		(0x00000001u)
#define UPP_UPIS2_ACT_SHIFT		(0x00000000u)


/* UPQD0 */

#define UPP_UPQD0_ADDRH		(0xFFFFFFF8u)
#define UPP_UPQD0_ADDRH_SHIFT		(0x00000003u)

#define UPP_UPQD0_ADDR		(0x00000007u)
#define UPP_UPQD0_ADDR_SHIFT		(0x00000000u)


/* UPQD1 */

#define UPP_UPQD1_LNCNT		(0xFFFF0000u)
#define UPP_UPQD1_LNCNT_SHIFT		(0x00000010u)

#define UPP_UPQD1_BCNTH		(0x0000FFFEu)
#define UPP_UPQD1_BCNTH_SHIFT		(0x00000001u)

#define UPP_UPQD1_BCNT		(0x00000001u)
#define UPP_UPQD1_BCNT_SHIFT		(0x00000000u)


/* UPQD2 */


#define UPP_UPQD2_LNOFFSETH	(0x0000FFF8u)
#define UPP_UPQD2_LNOFFSETH_SHIFT	(0x00000003u)

#define UPP_UPQD2_LNOFFSET	(0x00000007u)
#define UPP_UPQD2_LNOFFSET_SHIFT	(0x00000000u)


/* UPQS0 */

#define UPP_UPQS0_ADDR		(0xFFFFFFFFu)
#define UPP_UPQS0_ADDR_SHIFT		(0x00000000u)


/* UPQS1 */

#define UPP_UPQS1_LNCNT		(0xFFFF0000u)
#define UPP_UPQS1_LNCNT_SHIFT		(0x00000010u)

#define UPP_UPQS1_BCNT		(0x0000FFFFu)
#define UPP_UPQS1_BCNT_SHIFT		(0x00000000u)


/* UPQS2 */


#define UPP_UPQS2_WM			(0x000000F0u)
#define UPP_UPQS2_WM_SHIFT		(0x00000004u)


#define UPP_UPQS2_PEND		(0x00000002u)
#define UPP_UPQS2_PEND_SHIFT		(0x00000001u)

#define UPP_UPQS2_ACT		(0x00000001u)
#define UPP_UPQS2_ACT_SHIFT		(0x00000000u)




#define SETBIT(dest,mask)	(dest |= mask)
#define CLRBIT(dest,mask)	(dest &= ~mask)
#define TGLBIT(dest,mask)	(dest ^= mask)
#define CHKBIT(dest,mask)	(dest & mask)

//registers
typedef struct
{
  volatile unsigned int UPPID;		// 0x0000
  volatile unsigned int UPPCR;		// 0x0004
  volatile unsigned int UPDLB;		// 0x0008
  volatile unsigned int NA1;		// 0x000c not accessable
  volatile unsigned int UPCTL;		// 0x0010
  volatile unsigned int UPICR;		// 0x0014
  volatile unsigned int UPIVR;		// 0x0018
  volatile unsigned int UPTCR;		// 0x001c
  volatile unsigned int UPISR;		// 0x0020
  volatile unsigned int UPIER;		// 0x0024
  volatile unsigned int UPIES;		// 0x0028
  volatile unsigned int UPIEC;		// 0x002c
  volatile unsigned int UPEOI;		// 0x0030
  volatile unsigned int NA2;		// 0x0034 not accessable
  volatile unsigned int NA3;		// 0x0038 not accessable
  volatile unsigned int NA4;		// 0x003c not accessable
  volatile unsigned int UPID0;		// 0x0040
  volatile unsigned int UPID1;		// 0x0044
  volatile unsigned int UPID2;		// 0x0048
  volatile unsigned int NA5;		// 0x004c not accessable
  volatile unsigned int UPIS0;		// 0x0050
  volatile unsigned int UPIS1;		// 0x0054
  volatile unsigned int UPIS2;		// 0x0058
  volatile unsigned int NA6;		// 0x005c not accessable
  volatile unsigned int UPQD0;		// 0x0060
  volatile unsigned int UPQD1;		// 0x0064
  volatile unsigned int UPQD2;		// 0x0068
  volatile unsigned int NA7;		// 0x007c not accessable
  volatile unsigned int UPQS0;		// 0x0070
  volatile unsigned int UPQS1;		// 0x0074
  volatile unsigned int UPQS2;		// 0x0078
} uPP_regs_t;

typedef struct
{
  //UPCTL
  union
  {
	struct 
	{
	unsigned MODE	:2;
	unsigned CHN		:1;
	unsigned SDRTXIL	:1;
	unsigned DDRDEMUX   :1;
	unsigned RESERVED3  :11;
	unsigned DRA		:1;
	unsigned IWA		:1;
	unsigned DPWA	:3;
	unsigned DPFA	:2;
	unsigned RESERVED2  :1;
	unsigned DRB		:1;
	unsigned IWB		:1;
	unsigned DPWB	:3;
	unsigned DPFB	:2;
	unsigned RESERVED1  :1;
	}bits; 
	unsigned int value;
  }UPCTL;
  
  //UPPCR
  union
  {
	struct
	{
	
		unsigned FREE		:1;
		unsigned SOFT		:1;
		unsigned RTEMU		:1;
		unsigned EN		:1;
		unsigned SWRST		:1;
		unsigned RESERVED2	:2;
		unsigned DB		:1;
		unsigned RESERVED1	:24;		
	}bits;  
	unsigned int value;
  }UPPCR;
  
  //UPDLB
  union 
  {
	struct
	{
	unsigned RESERVED2	:12;
	unsigned AB		:1;
	unsigned BA		:1;
	unsigned RESERVED1	:18;
	}bits;
	unsigned int value;
  }UPDLB;
  
  //UPICR
  union
  {
	struct
	{
	unsigned STARTPOLA	:1;
	unsigned ENAPOLA		:1;
	unsigned WAITPOLA	:1;
	unsigned STARTA		:1;
	unsigned ENAA		:1;
	unsigned WAITA		:1;
	unsigned RESERVED4	:2;
	unsigned CLKDIVA		:4;
	unsigned CLKINVA		:1;
	unsigned TRISA		:1;
	unsigned RESERVED3	:2;
	unsigned STARTPOLB	:1;
	unsigned ENAPOLB		:1;
	unsigned WAITPOLB	:1;
	unsigned STARTB		:1;
	unsigned ENAB		:1;
	unsigned WAITB		:1;
	unsigned RESERVED2	:2;
	unsigned CLKDIVB		:4;
	unsigned CLKINVB		:1;
	unsigned TRISB		:1;
	unsigned RESERVED1	:2;
	}bits;
	unsigned int value;
  }UPICR;
  
  //UPIVR
  union
  {
	struct
	{
	unsigned VALA			:16;
	unsigned VALB			:16;
	}bits;
	unsigned int value;
  }UPIVR;

  //UPTCR
  union
  {
	struct
	{
	unsigned RDSIZEI		:2;
	unsigned RESERVED4	:6;
	unsigned RDSIZEQ		:2;
	unsigned RESERVED3	:6;
	unsigned TXSIZEA		:2;
	unsigned RESERVED2	:6;
	unsigned TXSIZEB		:2;
	unsigned RESERVED1	:6;
	}bits;
	unsigned int value;
  }UPTCR;
  
  //UPIES
  union
  {
	struct
	{
	unsigned DPEI		:1;
	unsigned UORI		:1;
	unsigned ERRI		:1;
	unsigned EOWI		:1;
	unsigned EOLI		:1;
	unsigned RESERVED2	:3;
	unsigned DPEQ		:1;
	unsigned UORQ		:1;
	unsigned ERRQ		:1;
	unsigned EOWQ		:1; 
	unsigned EOLQ		:1;
	unsigned RESERVED1	:19;
	} bits;
	unsigned int value;
  }UPIES;
} upp_config_t;

//UPISR_t
typedef union
{
  struct
  {
	unsigned DPEI		:1;
	unsigned UORI		:1;
	unsigned ERRI		:1;
	unsigned EOWI		:1;
	unsigned EOLI		:1;
	unsigned RESERVED2	:3;
	unsigned DPEQ		:1;
	unsigned UORQ		:1;
	unsigned ERRQ		:1;
	unsigned EOWQ		:1; 
	unsigned EOLQ		:1;
	unsigned RESERVED1	:19;
  } bits;
  unsigned int value;
}UPISR_t;

typedef union
{
	struct
	{
		unsigned ACT		:1;		
		unsigned PEND		:1;
		unsigned RESERVED2	:2;
		unsigned WM			:4;
		unsigned RESERVED1	:24;
	}bits;
	unsigned int value;
}UPXS2_t;
#define UPP_REG_BASE		(0x01E16000)
#define UPP  ((uPP_regs_t *)UPP_REG_BASE)
#define uPPConfigPin (SYSCFG_PINMUX13_PINMUX13_19_16_CH1_CLK << SYSCFG_PINMUX13_PINMUX13_19_16_SHIFT) | \
					 (SYSCFG_PINMUX13_PINMUX13_23_20_CH1_START << SYSCFG_PINMUX13_PINMUX13_23_20_SHIFT) | \
					 (SYSCFG_PINMUX13_PINMUX13_27_24_CH1_ENABLE << SYSCFG_PINMUX13_PINMUX13_27_24_SHIFT) | \
                     (SYSCFG_PINMUX13_PINMUX13_31_28_CH1_WAIT << SYSCFG_PINMUX13_PINMUX13_31_28_SHIFT)
					
#define uPPDataPin   (SYSCFG_PINMUX16_PINMUX16_7_4_UPP_D0 << SYSCFG_PINMUX16_PINMUX16_7_4_SHIFT)


//-----------------------------------------------------------------------------
// Public Typedefs
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Public Function Prototypes
//-----------------------------------------------------------------------------
void UPP_init(void);
void uPPPinMuxSetup(void);
void SendUppToPLD(unsigned int BufAddr, unsigned int Length);
void MCBSP_Init();
void mcbsp0_write(unsigned char out_data);
#endif

