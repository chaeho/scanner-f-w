#ifndef __FLASH_H__
#define __FLASH_H__


#define FLASH_PAGE_SIZE					(256)
#define FLASH_SECTOR_SIZE				(4096)
#define MAX_FLASH_WRITE_SIZE			(FLASH_PAGE_SIZE)
#define MAX_FLASH_READ_SIZE				(FLASH_PAGE_SIZE)

void FlashOpen(void);
void FlashSectorErase(unsigned int Addr);
void FlashWrite(unsigned int Addr, unsigned char *pData, unsigned int length);
void FlashRead(unsigned int Addr, unsigned char *pData, unsigned int length);


#endif
