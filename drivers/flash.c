#include <string.h>
#include "soc_AM1808.h"
#include "evmAM1808.h"
#include "interrupt.h"

#include "spi_io.h"
#include "flash.h"


/* Serial NOR flash command list */
#define FLASH_CMD_WREN							(0x06)
#define FLASH_CMD_PP							(0x02)
#define FLASH_CMD_READ							(0x03)
#define FLASH_CMD_SE							(0x20)
#define FLASH_CMD_BE							(0xD8)
#define FLASH_CMD_RDSR							(0x05)
#define FLASH_CMD_ULBPR							(0x98)
#define FLASH_CMD_JEDEC_ID						(0x9F)

/* Serial NOR flash status register bit definition */
#define FLASH_STATUS_BUSY						(1 << 0)
#define FLASH_STATUS_WEL						(1 << 1)
#define FLASH_STATUS_WSE						(1 << 2)
#define FLASH_STATUS_WSP						(1 << 3)
#define FLASH_STATUS_WPLD						(1 << 4)
#define FLASH_STATUS_SEC						(1 << 5)


unsigned int	        flag;
unsigned int			len;

unsigned char			gSPITXBuf[MAX_FLASH_WRITE_SIZE + 4];
unsigned char			gSPIRXBuf[MAX_FLASH_READ_SIZE + 4];
unsigned int			gSPITXBufIdx;
unsigned int			gSPIRXBufIdx;


/* local function prototype */
void FlashInit(void);

void FlashSendCMD(void);
void FlashWriteEnable(void);
void FlashCheckBusy(void);

static void FlashIsr(void);

/* function definition */
void FlashInit(void)
{
    /* interrupt setup */
    IntRegister(SYS_INT_SPINT1, FlashIsr);
    IntChannelSet(SYS_INT_SPINT1, 8);
    IntSystemEnable(SYS_INT_SPINT1);
    SPIIntLevelSet(SOC_SPI_1_REGS, SPI_RECV_INTLVL | SPI_TRANSMIT_INTLVL);
    SPIIntEnable(SOC_SPI_1_REGS, (SPI_RECV_INT | SPI_TRANSMIT_INT));
}

static void FlashIsr(void)
{
	unsigned long intCode;	

	do
	{
		intCode = SPIInterruptVectorGet(SOC_SPI_1_REGS);

		if (intCode == SPI_TX_BUF_EMPTY)
		{
			if (len > 0)
			{
				len--;
				SPITransmitData1(SOC_SPI_1_REGS, gSPITXBuf[gSPITXBufIdx]);
				gSPITXBufIdx++;
			}
			if (!len)
			{
				SPIIntDisable(SOC_SPI_1_REGS, SPI_TRANSMIT_INT);
				gSPITXBufIdx = 0;
			}
		}

		if (intCode == SPI_RECV_FULL)
		{
			gSPIRXBuf[gSPIRXBufIdx] = (char)SPIDataReceive(SOC_SPI_1_REGS);
			gSPIRXBufIdx++;
			if (!len)
			{
				flag = 0;
				SPIIntDisable(SOC_SPI_1_REGS, SPI_RECV_INT);
				gSPIRXBufIdx = 0;
			}
		}
	} while (intCode);

    IntSystemStatusClear(SYS_INT_SPINT1);
}

void FlashSendCMD(void)
{
    flag = 1;

	SPIDat1Config(SOC_SPI_1_REGS, (SPI_CSHOLD | SPI_DATA_FORMAT0), SPI_CHIP_SELECT_HIGH);
	SPIIntEnable(SOC_SPI_1_REGS, (SPI_RECV_INT | SPI_TRANSMIT_INT));
    
	while (flag);
	
	/* Deasserts the CS pin(line) */
	SPIDat1Config(SOC_SPI_1_REGS, SPI_DATA_FORMAT0, SPI_CHIP_SELECT_HIGH);
}

void FlashWriteEnable(void)
{
	gSPITXBuf[0]	= FLASH_CMD_WREN;
	len				= 1;
	FlashSendCMD();
}

void FlashCheckBusy(void)
{
	gSPITXBuf[0] = FLASH_CMD_RDSR;
	do
	{
		len = 2;
		FlashSendCMD();
	} while (gSPIRXBuf[1] & FLASH_STATUS_BUSY);
}

void FlashOpen(void)
{
    FlashInit();

	/* send  */
	gSPITXBuf[0]	= FLASH_CMD_JEDEC_ID;
	gSPITXBuf[1]	= 0;
	gSPITXBuf[2]	= 0;
	gSPITXBuf[3]	= 0;
	len = 4;
	FlashSendCMD();

	/* send write enable */
	FlashWriteEnable();

	/* send unlock global write protect */
	gSPITXBuf[0]	= FLASH_CMD_ULBPR;
	len				= 1;
	FlashSendCMD();
}

void FlashSectorErase(unsigned int Addr)
{
	/* send write enable */
	FlashWriteEnable();

	/* send sector erase */
	gSPITXBuf[0]	= FLASH_CMD_SE;
	gSPITXBuf[1]	= (Addr >> 16) & 0xFF;
	gSPITXBuf[2]	= (Addr >> 8) & 0xFF;
	gSPITXBuf[3]	= Addr & 0xFF;
	len				= 4;
	FlashSendCMD();
	FlashCheckBusy();
}

void FlashWrite(unsigned int Addr, unsigned char *pData, unsigned int length)
{
	/* send write enable */
	FlashWriteEnable();

	/* send page program */
	gSPITXBuf[0]	= FLASH_CMD_PP;
	gSPITXBuf[1]	= (Addr >> 16) & 0xFF;
	gSPITXBuf[2]	= (Addr >> 8) & 0xFF;
	gSPITXBuf[3]	= Addr & 0xFF;
	len				= length + 4;
	memcpy(gSPITXBuf + 4, pData, length);
	FlashSendCMD();
	FlashCheckBusy();
}

void FlashRead(unsigned int Addr, unsigned char *pData, unsigned int length)
{
	/* send read memory */
	gSPITXBuf[0]    = FLASH_CMD_READ;
	gSPITXBuf[1]    = (Addr >> 16) & 0xFF;
	gSPITXBuf[2]    = (Addr >> 8) & 0xFF;
	gSPITXBuf[3]    = Addr & 0xFF;
	len             = length + 4;
	FlashSendCMD();

	memcpy(pData, gSPIRXBuf + 4, length);
}