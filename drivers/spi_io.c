#include "soc_AM1808.h"
#include "interrupt.h"
#include "evmAM1808.h"
#include "spi_io.h"


/* transive and receive */
unsigned int SPI0Transfer16b(unsigned int DeviceNum, unsigned int Data, unsigned int Flag)
{
    unsigned int Reg;

    /* TX busy check */
    while (SPIIntStatus(SOC_SPI_0_REGS, SPI_SPIFLG_TXINTFLG) == 0);
    SPIIntStatusClear(SOC_SPI_0_REGS, SPI_SPIFLG_TXINTFLG);

    /* CS pin assertion */
    SPIDefaultCSSet(SOC_SPI_0_REGS, 1 << DeviceNum);
    Reg = SPI_SPIPC0_SOMIFUN | SPI_SPIPC0_SIMOFUN | SPI_SPIPC0_CLKFUN | 1 << DeviceNum;
    SPIPinControl(SOC_SPI_0_REGS, 0, 0, &Reg);

    /* TX data transfer */
    SPITransmitData1(SOC_SPI_0_REGS, Data);

    /* wait for RX data alive */
    if ((Flag & SPI_RECV_INT) > 0)
    {
        while (SPIIntStatus(SOC_SPI_0_REGS, SPI_SPIFLG_RXINTFLG) == 0);
        SPIIntStatusClear(SOC_SPI_0_REGS, SPI_SPIFLG_RXINTFLG);
    }

    /* take RX data */
    Reg = SPIDataReceive(SOC_SPI_0_REGS);

    /* CS pin deassertion */
    SPIDat1Config(SOC_SPI_0_REGS, DeviceNum, 1 << DeviceNum);

    return Reg;
}

/*
 * API channel 0 configuration
 * slave 0 : Analog to Digital Converter
 * slave 1 : Analog Front End
 */
void SetUpSPI0(void)
{
    unsigned int  val;

    /* SPI master reset */
    SPIReset(SOC_SPI_0_REGS);
    SPIOutOfReset(SOC_SPI_0_REGS);
    SPIModeConfigure(SOC_SPI_0_REGS, SPI_MASTER_MODE);

    /* clock setup for each slave */
    SPIClkConfigure(SOC_SPI_0_REGS, 150000000, 20000000, SPI_DATA_FORMAT1);
    SPIClkConfigure(SOC_SPI_0_REGS, 150000000, 2000000, SPI_DATA_FORMAT0);
    SPIConfigClkFormat(SOC_SPI_0_REGS, (SPI_CLK_POL_HIGH | SPI_CLK_INPHASE), SPI_DATA_FORMAT1);
    SPIConfigClkFormat(SOC_SPI_0_REGS, (SPI_CLK_POL_LOW | SPI_CLK_OUTOFPHASE), SPI_DATA_FORMAT0);

    /* interface pin setup */
    val = SPI_SPIPC0_SOMIFUN |
          SPI_SPIPC0_SIMOFUN |
          SPI_SPIPC0_CLKFUN |
          1 << SPI0_DEVICE_ID_ADC |
          1 << SPI0_DEVICE_ID_AFE;
    SPIPinControl(SOC_SPI_0_REGS, 0, 0, &val);
    SPIPinMuxSetup(0);
    SPI0CSPinMuxSetup(0);
    SPI0CSPinMuxSetup(1);
    SPIDefaultCSSet(SOC_SPI_0_REGS, (1 << SPI0_DEVICE_ID_ADC | 1 << SPI0_DEVICE_ID_AFE));

    /* data width setup for each slave */
    SPIShiftMsbFirst(SOC_SPI_0_REGS, SPI_DATA_FORMAT0);
    SPICharLengthSet(SOC_SPI_0_REGS, 16, SPI_DATA_FORMAT0);
    SPIShiftMsbFirst(SOC_SPI_0_REGS, SPI_DATA_FORMAT1);
    SPICharLengthSet(SOC_SPI_0_REGS, 16, SPI_DATA_FORMAT1);

    /* master activate */
    SPIEnable(SOC_SPI_0_REGS);
}

void SetUpSPI1(void)
{
    unsigned int  val;

    /* SPI master reset */
    SPIReset(SOC_SPI_1_REGS);
    SPIOutOfReset(SOC_SPI_1_REGS);
    SPIModeConfigure(SOC_SPI_1_REGS, SPI_MASTER_MODE);

    /* clock setup for slave */
    SPIClkConfigure(SOC_SPI_1_REGS, 150000000, 20000000, SPI_DATA_FORMAT0);
    SPIConfigClkFormat(SOC_SPI_1_REGS, (SPI_CLK_POL_HIGH | SPI_CLK_INPHASE), SPI_DATA_FORMAT0);

    val = SPI_SPIPC0_SOMIFUN |
          SPI_SPIPC0_SIMOFUN |
          SPI_SPIPC0_CLKFUN |
          0x01;
    /* pin setup */
    SPIPinControl(SOC_SPI_1_REGS, 0, 0, &val);
    SPIPinMuxSetup(1);
    SPI1CSPinMuxSetup(0);
    SPIDefaultCSSet(SOC_SPI_1_REGS, 1);

    /* data width setup */
    SPIShiftMsbFirst(SOC_SPI_1_REGS, SPI_DATA_FORMAT0);
    SPICharLengthSet(SOC_SPI_1_REGS, 8, SPI_DATA_FORMAT0);

    /* master activate */
    SPIEnable(SOC_SPI_1_REGS);
}