#include "sysup.h"
#include "interrupt.h"
#include "delay.h"
#include "usb_bulk_structs.h"
#include "uart.h"
#include "spi_io.h"
#include "controller.h"


/* Function definition */
int main(void)
{
	/* System init */
    SDRAMInit();
	CopyVectorTable();
	SetupIntc();
	PSCModuleEnable();
	DelayTimerSetup();
	USBInit();
	UARTInit();
    SetUpSPI0();
    SetUpSPI1();

	/* Main logic init */
	CTRLInit();

	/* System run */
    CTRLRun();
}
