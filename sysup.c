#include "hw_types.h"
#include "soc_AM1808.h"
#include "hw_syscfg0_AM1808.h"
#include "psc.h"
#include "interrupt.h"
#include "gpio.h"

#define VECTOR_TABLE_LENGTH	        (14)

/** Vector Table **/
extern void UndefInstHandler(void);
extern void SWIHandler(void);
extern void AbortHandler(void);
extern void IRQHandler(void);
extern void FIQHandler(void);

const unsigned int vecTbl[VECTOR_TABLE_LENGTH] =
{
	0xE59FF018,
	0xE59FF018,
	0xE59FF018,
	0xE59FF018,
	0xE59FF014,
	0xE24FF008,
	0xE59FF010,
	0xE59FF010,
	(unsigned int)UndefInstHandler,
	(unsigned int)SWIHandler,
	(unsigned int)AbortHandler,
	(unsigned int)IRQHandler,
	(unsigned int)FIQHandler
};

void CopyVectorTable(void)
{
	unsigned int  *dst = (unsigned int *)0xFFFF0000;
	unsigned int  *src = (unsigned int *)vecTbl;
	unsigned int  cnt;

	for (cnt = 0; cnt < VECTOR_TABLE_LENGTH; cnt++)
	{
		dst[cnt] = src[cnt];
	}
}

void SetupIntc(void)
{
	/* Initialize the ARM Interrupt Controller.*/
	IntAINTCInit();

	/* Enable IRQ in CPSR.*/
	IntMasterIRQEnable();

	/* Enable the interrupts in GER of AINTC.*/
	IntGlobalEnable();

	/* Enable the interrupts in HIER of AINTC.*/
	IntIRQEnable();
}

void PSCModuleEnable(void)
{
	PSCModuleControl(SOC_PSC_1_REGS, HW_PSC_VPIF, PSC_POWERDOMAIN_ALWAYS_ON, PSC_MDCTL_NEXT_ENABLE);
	PSCModuleControl(SOC_PSC_1_REGS, HW_PSC_GPIO, PSC_POWERDOMAIN_ALWAYS_ON, PSC_MDCTL_NEXT_ENABLE);
	PSCModuleControl(SOC_PSC_0_REGS, HW_PSC_SPI0, PSC_POWERDOMAIN_ALWAYS_ON, PSC_MDCTL_NEXT_ENABLE);
	PSCModuleControl(SOC_PSC_1_REGS, HW_PSC_SPI1, PSC_POWERDOMAIN_ALWAYS_ON, PSC_MDCTL_NEXT_ENABLE);
	PSCModuleControl(SOC_PSC_1_REGS, HW_PSC_UART2, PSC_POWERDOMAIN_ALWAYS_ON, PSC_MDCTL_NEXT_ENABLE);
}

void SDRAMInit(void)
{
    HWREG(0x68000008) = 0x00004522; // SDCR register, 16 SDRAM bus mode[14], 2 EMA_CLK cycles[10], BIT11_9LOCK[8] enable
}